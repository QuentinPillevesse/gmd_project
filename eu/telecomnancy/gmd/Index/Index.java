package eu.telecomnancy.gmd.Index;

/*
Update the index when necessary
 */

import eu.telecomnancy.gmd.OMIMAnalysis.createIndexOMIM;
import eu.telecomnancy.gmd.XMLDataProcess.XMLParser;
import eu.telecomnancy.gmd.XMLDataProcess.createIndexXML;

import java.io.File;

public class Index {

    public Index() {
        this.check_Drugbank_index(new File("drugbank.xml"),new File("index/XML/index_updated.date"));
        this.check_OMIM_index(new File("omim.txt"),new File("index/OMIM/index_updated.date"));
    }

    /* Check the drugbank index and update if necessary */
    public void check_Drugbank_index(File file,File index){
        if(file.lastModified()>index.lastModified()){
            System.out.println("Drugbank index needs to be update");
            System.out.println("Indexing the XML document ...");
            XMLParser parser = new XMLParser(file);
            parser.toParse();
            createIndexXML createIndexXML=new createIndexXML(parser);
            createIndexXML.createIndex();
            System.out.println("Indexing done !");
        }else{
            System.out.println("Drugbank index up to date");
        }
    }

    /* Check the OMIM index and update if necessary */
    public void check_OMIM_index(File file,File index){
        if(file.lastModified()>index.lastModified()){
            System.out.println("OMIM index needs to be update");
            System.out.println("Indexing the document ...");
            createIndexOMIM createIndexOMIM=new createIndexOMIM();
            createIndexOMIM.createIndex();
            System.out.println("Indexing done !");
        }else{
            System.out.println("OMIM index up to date");
        }
    }
}

package eu.telecomnancy.gmd.ORPHA_DATA_Search;

import eu.telecomnancy.gmd.objects.Disease;
import eu.telecomnancy.gmd.objects.Symptom;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class URLConnection {

    private ArrayList<Integer> idList;
    private ArrayList<Symptom> clinicalSignsList=new ArrayList<Symptom>();
    private ArrayList<Disease> disease = new ArrayList<Disease>();
    private ArrayList<String> nameList;

    public void ConnectDisease(String name) throws Exception {

        idList = new ArrayList<Integer>();

        name = URLEncode(name);
        System.out.println("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseasesByName?key="+ name);
        URL diseaseByName = new URL("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseasesByName?key="+ name);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(diseaseByName.openStream()));


        String inputLine;
        String file="";
        while ((inputLine = in.readLine()) != null) {
            //System.out.println(inputLine);
            file = file + inputLine;
        }
        in.close();

        JSONObject obj = new JSONObject(file);

        JSONArray arr = obj.getJSONArray("rows");
        for (int i = 0; i < arr.length(); i++) {
            int post_id = arr.getJSONObject(i).getJSONObject("value").getInt("OrphaNumber");
            idList.add(post_id);
            //System.out.println(post_id);
        }

    }

    public void ConnectClinicalSigns(String disease) throws Exception {
        if(idList.size()>0) {
            int id = idList.get(0);


            System.out.println("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseClinicalSigns?key=[" + id + ",%22en%22]");
            URL clinicalSigns = new URL("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseClinicalSigns?key=[" + id + ",%22en%22]");
            BufferedReader in2 = new BufferedReader(
                    new InputStreamReader(clinicalSigns.openStream()));


            String inputLine2;
            String file2 = "";
            while ((inputLine2 = in2.readLine()) != null) {
                //System.out.println(inputLine2);
                file2 = file2 + inputLine2;
            }
            in2.close();

            JSONObject obj2 = new JSONObject(file2);

            JSONArray arr2 = obj2.getJSONArray("rows");
            for (int i = 0; i < arr2.length(); i++) {
                JSONObject jobj1 = arr2.getJSONObject(i).getJSONObject("value");
                JSONObject jobj2 = jobj1.getJSONObject("clinicalSign");
                JSONObject jobj3 = jobj2.getJSONObject("Name");
                String clinical = jobj3.getString("text");
                clinicalSignsList.add(new Symptom(clinical,"orpha",disease));
            }
        }
    }

    public String URLEncode(String url) {
        String Fields[] = url.split(" ");
        String urlFinal="%22";
        int i=0;
        for (i=0; i<Fields.length-1; i++) {
            urlFinal=urlFinal+Fields[i]+"%20";
        }
        urlFinal=urlFinal+Fields[Fields.length-1]+"%22";
        return urlFinal;
    }

    public ArrayList<Symptom> getClinicalSignsList() {
        return clinicalSignsList;
    }

    public ArrayList<Disease> getDiseasesList() {
        return this.disease;
    }

    public void ConnectSymptoms(String symptom) throws Exception {

        String symptom_name;
        ArrayList<String> symptoms = new ArrayList<String>();
        ArrayList<String> matchedSymptoms = new ArrayList<String>();

            symptom = URLEncode(symptom);
            System.out.println("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseByClinicalSign?key=" + symptom);
            URL diseaseBySymptom2 = new URL("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseByClinicalSign?key=" + symptom);
            BufferedReader in2 = new BufferedReader(
                    new InputStreamReader(diseaseBySymptom2.openStream()));


            String inputLine2;
            String file2="";
            while ((inputLine2 = in2.readLine()) != null) {
                //System.out.println(inputLine);
                file2 = file2 + inputLine2;
            }
        in2.close();

        JSONObject obj2 = new JSONObject(file2);

        JSONArray arr2 = obj2.getJSONArray("rows");
        for (int j = 0; j < arr2.length(); j++) {
            JSONObject jobj1 = arr2.getJSONObject(j).getJSONObject("value");
            JSONObject jobj2 = jobj1.getJSONObject("disease");
            JSONObject jobj3 = jobj2.getJSONObject("Name");
            String diseaseName = jobj3.getString("text");
            boolean isAlready = false;
            for (int i=0; i<disease.size(); i++) {
                if (!disease.get(i).getName().equals(diseaseName)) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    break;
                }
            }
            if (isAlready == false) {
                disease.add(new Disease(diseaseName,"orpha"));
            }
        }
    }

    public void ConnectSymptoms2(String symptom) throws Exception {

        String symptom_name;
        ArrayList<String> symptoms = new ArrayList<String>();
        ArrayList<String> matchedSymptoms = new ArrayList<String>();

        nameList = new ArrayList<String>();
        String subname2 = null;
        boolean hasTwoPart = false;
        String tmp[] = new String[0];
        if (symptom.indexOf('*') != -1) {
            tmp = symptom.split("\\*");
        } else if (symptom.indexOf('?') != -1) {
            tmp = symptom.split("\\?");
        }

        if (tmp.length == 1) {
            hasTwoPart = false;
        } else {
            hasTwoPart = true;
        }

        String subname = URLEncode(tmp[0]);
        String subname3 = URLEncode(tmp[0] + "%5Cufff0");
        if (hasTwoPart) {
            subname2 = tmp[1];
        }

        symptom = URLEncode(symptom);
        System.out.println("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseByClinicalSign?startkey=" + subname + "&endkey=" + subname3);
        URL diseaseBySymptom2 = new URL("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseByClinicalSign?startkey=" + subname + "&endkey=" + subname3);
        BufferedReader in2 = new BufferedReader(
                new InputStreamReader(diseaseBySymptom2.openStream()));


        String inputLine2;
        String file2 = "";
        while ((inputLine2 = in2.readLine()) != null) {
            //System.out.println(inputLine);
            file2 = file2 + inputLine2;
        }
        in2.close();

        JSONObject obj2 = new JSONObject(file2);

        JSONArray arr2 = obj2.getJSONArray("rows");
        for (int j = 0; j < arr2.length(); j++) {
            String diseaseNameTmp = arr2.getJSONObject(j).getString("key");
            if (hasTwoPart == true) {
                if (diseaseNameTmp.endsWith(subname2)) {
                    JSONObject jobj1 = arr2.getJSONObject(j).getJSONObject("value");
                    JSONObject jobj2 = jobj1.getJSONObject("disease");
                    JSONObject jobj3 = jobj2.getJSONObject("Name");
                    String diseaseName = jobj3.getString("text");
                    boolean isAlready = false;
                    for (int i = 0; i < disease.size(); i++) {
                        if (!disease.get(i).getName().equals(diseaseName)) {
                            isAlready = false;
                        } else {
                            isAlready = true;
                            break;
                        }
                    }
                    if (isAlready == false) {
                        disease.add(new Disease(diseaseName, "orpha"));
                    }
                }
            } else {
                JSONObject jobj1 = arr2.getJSONObject(j).getJSONObject("value");
                JSONObject jobj2 = jobj1.getJSONObject("disease");
                JSONObject jobj3 = jobj2.getJSONObject("Name");
                String diseaseName = jobj3.getString("text");
                boolean isAlready = false;
                for (int i = 0; i < disease.size(); i++) {
                    if (!disease.get(i).getName().equals(diseaseName)) {
                        isAlready = false;
                    } else {
                        isAlready = true;
                        break;
                    }
                }
                if (isAlready == false) {
                    disease.add(new Disease(diseaseName, "orpha"));
                }
            }
        }
    }

    public void ConnectDisease2(String name) throws Exception {
        idList = new ArrayList<Integer>();
        nameList = new ArrayList<String>();
        String subname2 = null;
        boolean hasTwoPart = false;
        String tmp[] = new String[0];
        if (name.indexOf('*')!=-1) {
            tmp = name.split("\\*");
        } else if (name.indexOf('?')!=-1) {
            tmp = name.split("\\?");
        }

        if (tmp.length==1) {
            hasTwoPart = false;
        } else {
            hasTwoPart = true;
        }

        String subname = URLEncode(tmp[0]);
        String subname3 = URLEncode(tmp[0]+"%5Cufff0");
        if (hasTwoPart) {
            subname2 = tmp[1];
        }

        System.out.println("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseasesByName?startkey="+ subname + "&endkey=" + subname3);
        URL diseaseByName = new URL("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseasesByName?startkey="+ subname +  "&endkey="  +subname3);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(diseaseByName.openStream()));


        String inputLine;
        String file="";
        while ((inputLine = in.readLine()) != null) {
            //System.out.println(inputLine);
            file = file + inputLine;
        }
        in.close();

        JSONObject obj = new JSONObject(file);

        JSONArray arr = obj.getJSONArray("rows");
        for (int i = 0; i < arr.length(); i++) {
            String diseaseName = arr.getJSONObject(i).getString("key");
            if (hasTwoPart==true ) {
                if (diseaseName.endsWith(subname2)) {
                    int post_id = arr.getJSONObject(i).getJSONObject("value").getInt("OrphaNumber");
                    idList.add(post_id);
                }
            } else {
                int post_id = arr.getJSONObject(i).getJSONObject("value").getInt("OrphaNumber");
                idList.add(post_id);
            }
            //System.out.println(post_id);
        }
    }

    public void ConnectClinicalSigns2(String disease) throws Exception {
        for (int j=0; j<idList.size(); j++) {
            int id = idList.get(j);


            System.out.println("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseClinicalSigns?key=[" + id + ",%22en%22]");
            URL clinicalSigns = new URL("http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseClinicalSigns?key=[" + id + ",%22en%22]");
            BufferedReader in2 = new BufferedReader(
                    new InputStreamReader(clinicalSigns.openStream()));


            String inputLine2;
            String file2 = "";
            while ((inputLine2 = in2.readLine()) != null) {
                //System.out.println(inputLine2);
                file2 = file2 + inputLine2;
            }
            in2.close();

            JSONObject obj2 = new JSONObject(file2);

            JSONArray arr2 = obj2.getJSONArray("rows");
            for (int i = 0; i < arr2.length(); i++) {
                JSONObject jobj1 = arr2.getJSONObject(i).getJSONObject("value");
                JSONObject jobj2 = jobj1.getJSONObject("clinicalSign");
                JSONObject jobj3 = jobj2.getJSONObject("Name");
                String clinical = jobj3.getString("text");
                clinicalSignsList.add(new Symptom(clinical,"orpha",disease));
            }
        }
    }

}
package eu.telecomnancy.gmd.XMLDataProcess;
/* Drug class which represents each Drug of the DrugBank file. */
public class Drug {

	private String name;
    private String indication;
    private String toxicity;

    public Drug() {       
    }

   public String getName() {
	   return this.name;
   }
   
   public String getIndication() {
	   return this.indication;
   }
   
   public String getToxicity() {
	   return this.toxicity;
   }
   
   public void setName(String name) {
	   this.name = name;
   }
   
   public void setIndication(String indication) {
	   this.indication = indication;
   }
   
   public void setToxicity(String toxicity) {
	   this.toxicity= toxicity;
   }
	
}

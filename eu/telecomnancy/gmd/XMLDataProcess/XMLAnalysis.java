package eu.telecomnancy.gmd.XMLDataProcess;

import java.io.File;
import java.util.ArrayList;

public class XMLAnalysis {
	
	XMLParser parser;

	ArrayList<Drug> drugList;
    ArrayList<Drug> drugUse = new ArrayList<Drug>();
    ArrayList<Drug> drugCause = new ArrayList<Drug>();
    ArrayList<String> diseaseTreated = new ArrayList<String>();
    
    //
    public XMLAnalysis() {
    	
    }
	
    // XML parsing and get the drugList from this parsing
	public void parseXML() {
        parser = new XMLParser(new File(""));
        parser.toParse();
        drugList =  parser.getList();
	}

	// Analyse the drugList and return the drugs which are used to treat the disease
	public ArrayList<Drug> cureAnalysis(String disease) {
		for (Drug drug : drugList) {
        	if (drug.getIndication().indexOf(disease)!=-1) {
        		drugUse.add(drug);
        	}
        }
    
        return drugUse;
	}
	
	// Analyse the drugList and return the drugs which cause the disease
	public ArrayList<Drug> adverseEffectsAnalysis(String disease) {
		for (Drug drug : drugList) {
			if (drug.getToxicity().indexOf(disease)!=-1) {
				drugCause.add(drug);
			}
		}
        
        return drugCause;		
	}
	
	// Return the list of diseases which are treated by a given medicine
	public ArrayList<String> diseaseTreatedList(String medecine) {
	
		for (Drug drug : drugList) {
			if (drug.getName().indexOf(medecine)!=-1) {
				diseaseTreated.add(drug.getIndication());
			}
		}
		
		return diseaseTreated;
		
	}
	
}

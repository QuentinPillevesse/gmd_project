package eu.telecomnancy.gmd.XMLDataProcess;


import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

public class createIndexXML {
    private XMLParser parser;

    public createIndexXML(XMLParser parser){
        this.parser=parser;
    }

    public void createIndex() {
        String indexPath = "./index/XML";

        Date start = new Date();
        try {
            System.out.println("Indexing to directory '" + indexPath + "'...");

            Directory dir = FSDirectory.open(Paths.get(indexPath));
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);


            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

            IndexWriter writer = new IndexWriter(dir, iwc);
            this.indexDoc(writer, this.parser.getList());

            // NOTE: if you want to maximize search performance,
            // you can optionally call forceMerge here.  This can be
            // a terribly costly operation, so generally it's only
            // worth it when your index is relatively static (ie
            // you're done adding documents to it):
            //
            // writer.forceMerge(1);

            writer.close();

            Date end = new Date();
            System.out.println(end.getTime() - start.getTime() + " total milliseconds");
            File f =new File(indexPath+"/index_updated.date");
            FileWriter fw = new FileWriter(f);
            fw.write(String.valueOf(new Date()));
            fw.close();

        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass() +
                    "\n with message: " + e.getMessage());
        }
    }

    /**
     * Indexes a single document
     */
    public void indexDoc(IndexWriter writer, ArrayList<Drug> drugList) throws IOException {


        Document doc = null;

        for(Drug drug:drugList){
            doc = new Document();
            doc.add(new Field("Name", drug.getName(), Field.Store.YES, Field.Index.ANALYZED));
            doc.add(new Field("Indication", drug.getIndication(), Field.Store.YES, Field.Index.ANALYZED));
            doc.add(new Field("Toxicity",  drug.getToxicity(), Field.Store.YES, Field.Index.ANALYZED));
            writer.addDocument(doc);
        }

        System.out.println("Finished");

    }
}
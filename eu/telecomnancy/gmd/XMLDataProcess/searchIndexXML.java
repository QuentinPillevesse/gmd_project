package eu.telecomnancy.gmd.XMLDataProcess;


import eu.telecomnancy.gmd.objects.AdverseEffectDrug;
import eu.telecomnancy.gmd.objects.DrugInfo;
import eu.telecomnancy.gmd.objects.TreatDrug;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

public class searchIndexXML {

    String index = "./index/XML";
    private String field ;




    // Analyse the drugList and return the drugs which are used to treat the disease
    public ArrayList<TreatDrug> cureAnalysis(String disease) throws IOException, ParseException {

        ArrayList<TreatDrug> drugUse = new ArrayList<>();
        field= "Indication";
        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String querystr = "\""+disease+"\"";
        Query q = new QueryParser(field, analyzer).parse(querystr);
        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;
        for(ScoreDoc result:results) {
            Document doc = indexSearcher.doc(result.doc);
            drugUse.add(new TreatDrug(doc.get("Name"),"Drugbank",disease));
        }
        return drugUse;
    }

    public ArrayList<TreatDrug> cureAnalysis2(String disease) throws IOException, ParseException {

        ArrayList<TreatDrug> drugUse = new ArrayList<>();
        field= "Indication";
        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String terms[]=disease.split(" ");
        BooleanQuery q = new BooleanQuery();
        for (int i=0; i<terms.length;i++) {
            Query q2 = new WildcardQuery(new Term(field, terms[i]));
            q.add(new BooleanClause(q2, BooleanClause.Occur.MUST));
        }
        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;
        for(ScoreDoc result:results) {
            Document doc = indexSearcher.doc(result.doc);
            drugUse.add(new TreatDrug(doc.get("Name"),"Drugbank",disease));
        }
        return drugUse;
    }

    // Analyse the drugList and return the drugs which cause the disease
    public ArrayList<AdverseEffectDrug> adverseEffectsAnalysis(String disease) throws IOException, ParseException {
         ArrayList<AdverseEffectDrug> drugUse = new ArrayList<>();
        field= "Toxicity";

        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String querystr = "\""+disease+"\"";
        Query q = new QueryParser(field, analyzer).parse(querystr);
        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;


        for(ScoreDoc result:results) {
            Document doc = indexSearcher.doc(result.doc);
            drugUse.add(new AdverseEffectDrug(doc.get("Name"),"Drugbank",disease));
        }
        return drugUse;
    }

    public ArrayList<AdverseEffectDrug> adverseEffectsAnalysis2(String disease) throws IOException, ParseException {
        ArrayList<AdverseEffectDrug> drugUse = new ArrayList<>();
        field= "Toxicity";

        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String terms[]=disease.split(" ");
        BooleanQuery q = new BooleanQuery();
        for (int i=0; i<terms.length;i++) {
            Query q2 = new WildcardQuery(new Term(field, terms[i]));
            q.add(new BooleanClause(q2, BooleanClause.Occur.MUST));
        }
        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;


        for(ScoreDoc result:results) {
            Document doc = indexSearcher.doc(result.doc);
            drugUse.add(new AdverseEffectDrug(doc.get("Name"),"Drugbank",disease));
        }
        return drugUse;
    }

    public ArrayList<DrugInfo> drugAnalysis(String drug) throws IOException, ParseException {
        ArrayList<DrugInfo> drugs = new ArrayList<>();
        field= "Name";

        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String querystr = "\""+drug+"\"";
        Query q = new QueryParser(field, analyzer).parse(querystr);
        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;

        for (ScoreDoc result : results) {
            Document doc = indexSearcher.doc(result.doc);
            String indication = doc.get("Indication");
            indication = indication.replaceAll("START ","").replaceAll(" STOP","");
            indication = indication.trim();

            String toxicity = doc.get("Toxicity");
            toxicity = toxicity.replaceAll("START ","").replaceAll(" STOP","");
            toxicity = toxicity.trim();

            DrugInfo drugInfos = new DrugInfo(doc.get("Name"),indication, toxicity);
            drugs.add(drugInfos);
        }


        return drugs;
    }

    public ArrayList<DrugInfo> drugAnalysis2(String drug) throws IOException, ParseException {
        ArrayList<DrugInfo> drugs = new ArrayList<>();
        field= "Name";

        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String terms[]=drug.split(" ");
        BooleanQuery q = new BooleanQuery();
        for (int i=0; i<terms.length;i++) {
            Query q2 = new WildcardQuery(new Term(field, terms[i]));
            q.add(new BooleanClause(q2, BooleanClause.Occur.MUST));
        }
        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;

        for (ScoreDoc result : results) {
                Document doc = indexSearcher.doc(result.doc);

                String indication = doc.get("Indication");
                indication = indication.replaceAll("START ","").replaceAll(" STOP","");

                indication = indication.trim();

                String toxicity = doc.get("Toxicity");
                toxicity = toxicity.replaceAll("START ","").replaceAll(" STOP","");

                toxicity = toxicity.trim();

                DrugInfo drugInfos = new DrugInfo(doc.get("Name"),indication, toxicity);
                drugs.add(drugInfos);

            }

        return drugs;
    }

}
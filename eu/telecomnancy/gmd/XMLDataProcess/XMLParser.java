package eu.telecomnancy.gmd.XMLDataProcess;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class XMLParser {
	private File file;

	ArrayList<Drug> drugList;
	public XMLParser(File file){
		this.file=file;
	}

	// Parse the XML document, creating Drug instanciations for each drug which is in
	public void toParse() {
		String text = null;
		Drug drug = null;
		int i=0;
		int j=0;
		int k=0;
		drugList = new ArrayList<Drug>();
		try {
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(file));

			while (reader.hasNext()) {
				int Event = reader.next();
				switch (Event) {
				case XMLStreamConstants.START_ELEMENT: {
					if ("drug".equals(reader.getLocalName())) {
						drug = new Drug();
					}
					break;
				}
				case XMLStreamConstants.CHARACTERS: {
					text = reader.getText().trim();
					break;
				}
				case XMLStreamConstants.END_ELEMENT: {
					switch (reader.getLocalName()) {
					case "drug": {
						drugList.add(drug);
						i=0;
						j=0;
						k=0;
						break;
					}
					case "name": {
						if (i==0) {
							drug.setName(text);
							i=i+1;
						}
						break;
					}
					case "indication": {
						if (j==0) {
							drug.setIndication(text);
							j=j+1;
						}
						break;
					}
					case "toxicity": {
						if (k==0) {
							drug.setToxicity(text);
							k=k+1;
							break;
						}
					}
					}
					break;
				}
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	// Return the list of drugs from the XML document
	public ArrayList<Drug> getList() {
		return drugList;
	}
}
package eu.telecomnancy.gmd.SIDER2_Search;

import eu.telecomnancy.gmd.objects.AdverseEffectDrug;
import eu.telecomnancy.gmd.objects.Disease;
import eu.telecomnancy.gmd.objects.TreatDrug;

import java.sql.*;
import java.util.ArrayList;

/*
This class allows us to access the Sider2 database and extract the datas
 */

public class DatabaseConnection {

   /* combination: two or more drugs were combined
    - not found: could not find the name in the database
    - mapping conflict: the available names point to two different compounds
    - template: a package insert that contains information for a group of related drugs
    */

    static String DRIVER = "com.mysql.jdbc.Driver";
    static String DB_SERVER = "jdbc:mysql://neptune.telecomnancy.univ-lorraine.fr/";
    static String DB = "gmd";
    static String LOGIN = "gmd-read";
    static String PWD = "esial";

    protected Connection con;



    PreparedStatement preparedSt = null;

    //Find name of drugs which are indications for a disease
    public ArrayList<TreatDrug> selectDrug(String name) {
        ArrayList<TreatDrug> treatDrugList = new ArrayList<>();
        ArrayList<ArrayList<String>> drugList = new ArrayList<>();
        ArrayList<String> drug = new ArrayList<>();
        try {

            /** Database connection **/
            Class.forName(DRIVER);


            Connection con = DriverManager.getConnection(DB_SERVER + DB, LOGIN, PWD);

            /** Get the datas from the database **/
            String SQL = "SELECT DISTINCT m.drug_name1,m.label FROM indications_raw i , label_mapping m WHERE i.i_name=? AND i.label=m.label AND m.drug_name1!='' AND m.marker=''  ";
            preparedSt = con.prepareStatement(SQL);
            preparedSt.setString(1, name);

            // Execute select SQL statement
            ResultSet res = preparedSt.executeQuery();


            while (res.next()) {
                if (!res.getString("drug_name1").isEmpty()) {
                    drug = new ArrayList<>();
                    drug.add(res.getString("drug_name1"));
                    drug.add(res.getString("m.label"));
                    drugList.add(drug);
                }
            }

            res.close();
            preparedSt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create the list of treat drugs
        for (ArrayList<String> data : drugList) {
            treatDrugList.add(new TreatDrug(data.get(0), "sider2",name));
        }
        return treatDrugList;
    }

    public ArrayList<TreatDrug> selectDrug2(String name) {
        ArrayList<TreatDrug> treatDrugList = new ArrayList<>();
        ArrayList<ArrayList<String>> drugList = new ArrayList<>();
        ArrayList<String> drug = new ArrayList<>();
        String nameDrug = name.replaceAll("\\*","%");
        nameDrug = nameDrug.replaceAll("\\?","_");

        try {

            /** Database connection **/
            Class.forName(DRIVER);


            Connection con = DriverManager.getConnection(DB_SERVER + DB, LOGIN, PWD);

            /** Get the datas from the database **/
            String SQL = "SELECT DISTINCT m.drug_name1,m.label FROM indications_raw i , label_mapping m WHERE i.i_name LIKE '"+nameDrug +"' AND i.label=m.label AND m.drug_name1!='' AND m.marker=''  ";
            preparedSt = con.prepareStatement(SQL);
            //preparedSt.setString(1, name);

            // Execute select SQL statement
            ResultSet res = preparedSt.executeQuery();


            while (res.next()) {
                if (!res.getString("drug_name1").isEmpty()) {
                    drug = new ArrayList<>();
                    drug.add(res.getString("drug_name1"));
                    drug.add(res.getString("m.label"));
                    drugList.add(drug);
                }
            }

            res.close();
            preparedSt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create the list of treat drugs
        for (ArrayList<String> data : drugList) {
            treatDrugList.add(new TreatDrug(data.get(0), "sider2",name));
        }
        return treatDrugList;
    }

    // Find name of the drugs which are an adverse effect of a disease
    public ArrayList<AdverseEffectDrug> selectDrugAdverse(String name) {
        ArrayList<AdverseEffectDrug> adverseEffectDrugList= new ArrayList<>();
        ArrayList<ArrayList<String>> drugList=new ArrayList<>();
        ArrayList<String> drug=new ArrayList<>();
        try {

            /** Database connection **/
            Class.forName(DRIVER);


            Connection con = DriverManager.getConnection(DB_SERVER + DB, LOGIN, PWD);

            /** Get the datas from the database **/
            String SQL = "SELECT DISTINCT m.drug_name1 FROM adverse_effects_raw a , label_mapping m WHERE a.se_name=? AND a.label=m.label AND m.marker='' ";
            preparedSt = con.prepareStatement(SQL);
            preparedSt.setString(1,name);

            // Execute select SQL statement
            ResultSet res = preparedSt.executeQuery();



            while (res.next()) {
                if (!res.getString("drug_name1").isEmpty()) {
                    drug = new ArrayList<>();
                    drug.add(res.getString("drug_name1"));

                    drugList.add(drug);
                }
            }

            res.close();
            preparedSt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (ArrayList<String> data : drugList) {
            adverseEffectDrugList.add(new AdverseEffectDrug(data.get(0), "sider2",name));
        }

        return adverseEffectDrugList;
    }

    public ArrayList<AdverseEffectDrug> selectDrugAdverse2(String name) {
        ArrayList<AdverseEffectDrug> adverseEffectDrugList= new ArrayList<>();
        ArrayList<ArrayList<String>> drugList=new ArrayList<>();
        ArrayList<String> drug=new ArrayList<>();
        String nameDrug = name.replaceAll("\\*","%");
        nameDrug = nameDrug.replaceAll("\\?","_");
        try {

            /** Database connection **/
            Class.forName(DRIVER);


            Connection con = DriverManager.getConnection(DB_SERVER + DB, LOGIN, PWD);

            /** Get the datas from the database **/
            String SQL = "SELECT DISTINCT m.drug_name1 FROM adverse_effects_raw a , label_mapping m WHERE a.se_name LIKE '"+nameDrug +"' AND a.label=m.label AND m.marker=''";
            preparedSt = con.prepareStatement(SQL);
            //preparedSt.setString(1,name);

            // Execute select SQL statement
            ResultSet res = preparedSt.executeQuery();



            while (res.next()) {
                if (!res.getString("drug_name1").isEmpty()) {
                    drug = new ArrayList<>();
                    drug.add(res.getString("drug_name1"));

                    drugList.add(drug);
                }
            }

            res.close();
            preparedSt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (ArrayList<String> data : drugList) {
            adverseEffectDrugList.add(new AdverseEffectDrug(data.get(0), "sider2",name));
        }

        return adverseEffectDrugList;
    }

    // maladies traitées par un medicaments

    public ArrayList<Disease> selectIndicationDisease(String drug){

        ArrayList<Disease> diseaseList=new ArrayList<>();

        try {

            /**Connexion à la base de données**/
            Class.forName(DRIVER);


            Connection con = DriverManager.getConnection(DB_SERVER + DB, LOGIN, PWD);

            /**Récupération des données dans la base**/
            String SQL = "SELECT DISTINCT i.i_name FROM indications_raw i , label_mapping m WHERE m.drug_name1=? AND i.label=m.label";
            preparedSt = con.prepareStatement(SQL);
            preparedSt.setString(1,drug);

            // execute select SQL stetement
            ResultSet res = preparedSt.executeQuery();



            while (res.next()) {
                if (!res.getString("i.i_name").isEmpty()) {
                    diseaseList.add(new Disease(res.getString("i.i_name"), "sider2"));
                }
            }

            res.close();
            preparedSt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diseaseList;
    }

    public ArrayList<Disease> selectIndicationDisease2(String drug){

        ArrayList<Disease> diseaseList=new ArrayList<>();

        String nameDrug = drug.replaceAll("\\*","%");
        nameDrug = nameDrug.replaceAll("\\?","_");

        try {

            /**Connexion à la base de données**/
            Class.forName(DRIVER);


            Connection con = DriverManager.getConnection(DB_SERVER + DB, LOGIN, PWD);

            /**Récupération des données dans la base**/
            String SQL = "SELECT DISTINCT i.i_name FROM indications_raw i , label_mapping m WHERE m.drug_name1 LIKE '"+nameDrug +"' AND i.label=m.label";
            preparedSt = con.prepareStatement(SQL);
            //preparedSt.setString(1,drug);

            // execute select SQL stetement
            ResultSet res = preparedSt.executeQuery();



            while (res.next()) {
                if (!res.getString("i.i_name").isEmpty()) {
                    diseaseList.add(new Disease(res.getString("i.i_name"), "sider2"));
                }
            }

            res.close();
            preparedSt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diseaseList;
    }

    public ArrayList<Disease> selectToxicityDisease(String drug){

        ArrayList<Disease> diseaseList=new ArrayList<>();

        try {

            /**Connexion à la base de données**/
            Class.forName(DRIVER);


            Connection con = DriverManager.getConnection(DB_SERVER + DB, LOGIN, PWD);

            /**Récupération des données dans la base**/
            String SQL = "SELECT DISTINCT a.se_name FROM adverse_effects_raw a , label_mapping m WHERE m.drug_name1=? AND a.label=m.label";
            preparedSt = con.prepareStatement(SQL);
            preparedSt.setString(1,drug);

            // execute select SQL stetement
            ResultSet res = preparedSt.executeQuery();



            while (res.next()) {
                if (!res.getString("a.se_name").isEmpty()) {
                    diseaseList.add(new Disease(res.getString("a.se_name"), "sider2"));
                }
            }

            res.close();
            preparedSt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diseaseList;
    }

    public ArrayList<Disease> selectToxicityDisease2(String drug){

        ArrayList<Disease> diseaseList=new ArrayList<>();

        String nameDrug = drug.replaceAll("\\*","%");
        nameDrug = nameDrug.replaceAll("\\?","_");

        try {

            /**Connexion à la base de données**/
            Class.forName(DRIVER);


            Connection con = DriverManager.getConnection(DB_SERVER + DB, LOGIN, PWD);

            /**Récupération des données dans la base**/
            String SQL = "SELECT DISTINCT a.se_name FROM adverse_effects_raw a , label_mapping m WHERE m.drug_name1 LIKE '"+nameDrug +"' AND a.label=m.label";
            preparedSt = con.prepareStatement(SQL);
            //preparedSt.setString(1,drug);

            // execute select SQL stetement
            ResultSet res = preparedSt.executeQuery();



            while (res.next()) {
                if (!res.getString("a.se_name").isEmpty()) {
                    diseaseList.add(new Disease(res.getString("a.se_name"), "sider2"));
                }
            }

            res.close();
            preparedSt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diseaseList;
    }
}






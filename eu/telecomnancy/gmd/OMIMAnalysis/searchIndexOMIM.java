package eu.telecomnancy.gmd.OMIMAnalysis;


import eu.telecomnancy.gmd.objects.Disease;
import eu.telecomnancy.gmd.objects.Symptom;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

public class searchIndexOMIM {

    String index = "./index/OMIM";
    private String field ;


    public ArrayList<Symptom> getClinicalSignsList2(String disease) throws IOException, ParseException {
        ArrayList<Symptom> symptomList= new ArrayList<>();
        field= "Name";
        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String terms[]=disease.split(" ");
        BooleanQuery q = new BooleanQuery();
        for (int i=0; i<terms.length;i++) {
            Query q2 = new WildcardQuery(new Term(field, terms[i]));
            q.add(new BooleanClause(q2, BooleanClause.Occur.MUST));
        }

        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;

        for(ScoreDoc result:results) {
            Document doc = indexSearcher.doc(result.doc);
            String symptoms = doc.get("Symptoms");
            if (symptoms!=null) {
                String fields[] = symptoms.split("  ");
                for (int i=0; i<fields.length; i++) {
                    if (fields[i].contains("caused") || fields[i].startsWith("gene") || fields[i].startsWith("(")) {

                    } else {
                        Symptom tmp = new Symptom(fields[i].trim(), "OMIM",disease.replaceAll("START ","").replaceAll(" STOP",""));
                        symptomList.add(tmp);
                    }
                }
            }
            //System.out.println(doc.get("Name"));
            //symptomList.add(new Symptom(doc.get("Symptoms"),"OMIM"));
        }

        return symptomList;

    }


    public ArrayList<Symptom> getClinicalSignsList(String disease) throws IOException, ParseException {

        ArrayList<Symptom> symptomList= new ArrayList<>();
        field= "Name";
        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String querystr = "\""+disease+"\"";
        System.out.println(querystr);
        Query q = new QueryParser(field, analyzer).parse(querystr);

        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;

        for(ScoreDoc result:results) {
            Document doc = indexSearcher.doc(result.doc);
            String symptoms = doc.get("Symptoms");
            if (symptoms!=null) {
                String fields[] = symptoms.split("  ");
                for (int i=0; i<fields.length; i++) {
                    if (fields[i].contains("caused") || fields[i].startsWith("gene") || fields[i].startsWith("(")) {

                    } else {
                        Symptom tmp = new Symptom(fields[i].trim(), "OMIM",disease.replaceAll("START ","").replaceAll(" STOP",""));
                        symptomList.add(tmp);
                    }
                }
            }
            System.out.println(doc.get("Name"));
            //symptomList.add(new Symptom(doc.get("Symptoms"),"OMIM"));
        }
        return symptomList;
    }

    public ArrayList<Disease> getDiseasesList(String symptom) throws IOException, ParseException {
        ArrayList<Disease> diseaseList= new ArrayList<Disease>();
        field= "Symptoms";
        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String querystr = "\""+symptom+"\"";
        System.out.println(querystr);
        Query q = new QueryParser(field, analyzer).parse(querystr);
        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;

        for(ScoreDoc result:results) {
            Document doc = indexSearcher.doc(result.doc);
            diseaseList.add(new Disease(doc.get("Name"),"OMIM"));
        }
        return diseaseList;
    }

    public ArrayList<Disease> getDiseasesList2(String symptom) throws IOException, ParseException {
        ArrayList<Disease> diseaseList= new ArrayList<Disease>();
        field= "Symptoms";
        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        Analyzer analyzer = new StandardAnalyzer();
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        String terms[]=symptom.split(" ");
        BooleanQuery q = new BooleanQuery();
        for (int i=0; i<terms.length;i++) {
            Query q2 = new WildcardQuery(new Term(field, terms[i]));
            q.add(new BooleanClause(q2, BooleanClause.Occur.MUST));
        }
        ScoreDoc[] results = indexSearcher.search(q,1000).scoreDocs;

        for(ScoreDoc result:results) {
            Document doc = indexSearcher.doc(result.doc);
            diseaseList.add(new Disease(doc.get("Name"),"OMIM"));
        }
        return diseaseList;
    }

}
package eu.telecomnancy.gmd.OMIMAnalysis;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.nio.file.Paths;
import java.util.Date;

/** eu.telecomnancy.gmd.Index all text files under a directory.
 * <p>
 * This is a command-line application demonstrating simple Lucene indexing.
 * Run it with no command-line arguments for usage information.
 */
public class createIndexOMIM {

    public createIndexOMIM() {
    }

    /**
     * eu.telecomnancy.gmd.Index all text files under a directory.
     */
    public void createIndex() {

        String indexPath = "./index/OMIM";
        String dbPath = "omim.txt";

        Date start = new Date();
        try {
            System.out.println("Indexing to directory '" + indexPath + "'...");

            Directory dir = FSDirectory.open(Paths.get(indexPath));
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);


            iwc.setOpenMode(OpenMode.CREATE);

            IndexWriter writer = new IndexWriter(dir, iwc);
            indexDoc(writer, dbPath);

            // NOTE: if you want to maximize search performance,
            // you can optionally call forceMerge here.  This can be
            // a terribly costly operation, so generally it's only
            // worth it when your index is relatively static (ie
            // you're done adding documents to it):
            //
            // writer.forceMerge(1);

            writer.close();

            Date end = new Date();
            System.out.println(end.getTime() - start.getTime() + " total milliseconds");
            File f = new File(indexPath + "/index_updated.date");
            FileWriter fw = new FileWriter(f);
            fw.write(String.valueOf(new Date()));
            fw.close();
        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass() +
                    "\n with message: " + e.getMessage());
        }
    }

    /**
     * Indexes a single document
     */
    public void indexDoc(IndexWriter writer, String path) throws IOException {

        File dbFile = new File(path);
        Document doc = null;
        String content;
        int count = 0;

        if (dbFile.canRead() && !dbFile.isDirectory()) {

            try {
                InputStream ips = new FileInputStream(dbFile);
                InputStreamReader ipsr = new InputStreamReader(ips);
                BufferedReader br = new BufferedReader(ipsr);
                String line;
                String info;
                String newinfo;
                doc = new Document();

                while ((line = br.readLine()) != null) {

                    if(line.length() != 0) {
                        String fields[] = line.split(" ");

                        if (!(fields[0].equals("*RECORD*"))) {

                            if (fields[0].equals("*FIELD*")) {

                                if (fields[1].equals("TI")) {
                                   info="";


                                    while (!(((line=br.readLine()).split(" "))[0].equals("*FIELD*"))){
                                        while (line.length() == 0){
                                            line = br.readLine();
                                        };
                                        info=info+" "+line;
                                    };
                                    info=info.split(";")[0];

                                    info = info.replaceAll("#", "");
                                    info = info.replaceAll("%", "");
                                    info = info.replaceAll("//~", "");
                                    info = info.replaceAll("^", "");
                                    info = info.replaceAll(";", "");
                                    info = info.replaceAll(",", "");
                                    String[] tempinfo= info.split(" ");
                                    newinfo=new String();
                                    for(int k=2;k<tempinfo.length;k++){
                                        newinfo=newinfo+" "+tempinfo[k];
                                    }

                                    newinfo="START "+newinfo.substring(1)+" STOP";
                                    doc.add(new Field("Name",newinfo, Field.Store.YES, Field.Index.ANALYZED));

                                } else if (fields[1].equals("CS")) {
                                    info="";
                                    while (!(((line=br.readLine()).split(" "))[0].equals("*FIELD*"))){
                                        while (line.matches("(.*):") || line.matches(".*\\[.*\\].*") ){
                                            line = br.readLine();
                                        };
                                        if(line.length() != 0) {
                                            info = info +line.substring(1);
                                        }
                                    };
                                    while ((line = br.readLine()).length() == 0) ;
                                    info = info.replaceAll("#", "");
                                    info = info.replaceAll("%", "");
                                    info = info.replaceAll("//~", "");
                                    info = info.replaceAll("^", "");
                                    info = info.replaceAll(";", "");
                                    info = info.replaceAll(",", "");
                                    String[] tempinfo= info.split(" ");
                                    newinfo=new String();
                                    for(int k=2;k<tempinfo.length;k++){
                                        newinfo=newinfo+" "+tempinfo[k];
                                    }

                                    doc.add(new TextField("Symptoms", newinfo, Field.Store.YES));
                                }
                            }
                        } else {
                            writer.addDocument(doc);
                            doc = new Document();
                        }
                    }
                }
                System.out.println("Finished");
                br.close();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
    }
}




            /*



    		while((line=br.readLine())!=null) {
    			String fields[] = line.split(" ");
    			if (fields[0].equals("*RECORD*")) {
    				if (count!=0) {
    					writer.addDocument(doc);
    				}
    				doc = new Document();
    				count=count+1;
    			}
    			if(fields[0].equals("*FIELD*") && fields.length>=2) {
    			switch (fields[1]) {
    	            case "TI":
    	            	content = "";
    	            	while((line=br.readLine()).length()>0 && !line.split(" ")[0].equals("*FIELD*")) {
    	            		if (line.charAt(0)=='*' || line.charAt(0)=='+') {
    	            			
    	            		} else {
    	            		
    	            			line=line.replaceAll("#", "");
    	            			line=line.replaceAll("%", "");
    	            			line=line.replaceAll("//~", "");
                                line=line.replaceAll("^", "");
    	            			content = content+" "+line;
    	            		}
    	            	}
                        String[] splits = content.split(";");
                        String content2 = splits[0];
                        String[] splits2 = content2.split(" ");
                        String content3="";
                        for (int i=2; i<splits2.length; i++) {



                            content3 = content3 + " " + splits2[i];
                        }
                        if (content3.contains(";")) {
                            content3 = content3.replace(";", "");
                        }
    	            	doc.add(new Field("Name", content3, Field.Store.YES, Field.eu.telecomnancy.gmd.Index.ANALYZED));
    	                break;
    	            case "CS":
    	            	content = "";
    	            	while((!(line=br.readLine()).split(" ")[0].equals("*FIELD*"))) {
							if (line.indexOf(":")==-1 && line.indexOf("[")==-1) {
                                content = content + " _ " + line;
							}
    	            	}
    	            	doc.add(new TextField("Symptoms", content, Field.Store.YES));
    	                break;  				
    			}
    			}
    		}
    		
    		System.out.println("Finished");
    		br.close();
    	} catch(Exception e) {
    		System.out.println(e.toString());
    	}
    	*/


  



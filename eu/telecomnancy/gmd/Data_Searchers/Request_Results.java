package eu.telecomnancy.gmd.Data_Searchers;

import eu.telecomnancy.gmd.objects.AdverseEffectDrug;
import eu.telecomnancy.gmd.objects.Symptom;
import eu.telecomnancy.gmd.objects.TreatDrug;

import java.util.ArrayList;

/**
 * Results of the request about diseases
 */

public class Request_Results {

    ArrayList<TreatDrug> treats;
    ArrayList<AdverseEffectDrug> adverseEffects;
    ArrayList<Symptom> symptoms;

    public Request_Results(ArrayList<TreatDrug> treats, ArrayList<AdverseEffectDrug> adverseEffects, ArrayList<Symptom> symptoms) {
        this.treats = treats;
        this.adverseEffects = adverseEffects;
        this.symptoms = symptoms;
    }

    public ArrayList<TreatDrug> getTreats() {
        return treats;
    }

    public ArrayList<AdverseEffectDrug> getAdverseEffects() {
        return adverseEffects;
    }

    public ArrayList<Symptom> getSymptoms() {
        return symptoms;
    }

    public void intersect(Request_Results tmp) {
        ArrayList<TreatDrug> treats2 = this.intersectTreats(tmp.getTreats());
        ArrayList<AdverseEffectDrug> adverseEffects2 = this.intersectAdverseEffectDrugs(tmp.getAdverseEffects());
        ArrayList<Symptom> symptoms2 = this.intersectSymptoms(tmp.getSymptoms());

        this.treats = treats2;
        this.adverseEffects = adverseEffects2;
        this.symptoms = symptoms2;
    }

    public ArrayList<TreatDrug> intersectTreats(ArrayList<TreatDrug> tmp_treats) {

        ArrayList<String> names1 = new ArrayList<>();
        ArrayList<String> names2 = new ArrayList<>();

        for (int i=0; i<this.treats.size(); i++) {
            names1.add(this.treats.get(i).getName());
        }
        for (int i=0; i<tmp_treats.size(); i++) {
            names2.add(tmp_treats.get(i).getName());
        }

        names1.retainAll(names2);
        names2.retainAll(names1);

        ArrayList<TreatDrug> treats2 = new ArrayList<>();
        ArrayList<TreatDrug> tmp_treats2 = new ArrayList<>();

        for (int i=0; i<this.treats.size(); i++) {
            for (int j=0; j<names1.size(); j++) {
                if (this.treats.get(i).getName().equals(names1.get(j))) {
                    treats2.add(this.treats.get(i));
                }
            }
        }

        for (int i=0; i<tmp_treats.size(); i++) {
            for (int j=0; j<names2.size(); j++) {
                if (tmp_treats.get(i).getName().equals(names2.get(j))) {
                    tmp_treats2.add(tmp_treats.get(i));
                }
            }
        }

        boolean isAlready = false;

        for (int i=0; i<tmp_treats2.size(); i++) {
            for (int j=0; j<treats2.size(); j++) {
                if (!treats2.get(j).getName().equals(tmp_treats2.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    for (String source:tmp_treats2.get(i).getSource()) {
                        if (!treats2.get(j).getSource().contains(source)) {
                            treats2.get(j).addSource(source);
                        }else{
                            treats2.get(j).addDisease(tmp_treats2.get(i).getDisease().get(0));
                        }
                    }
                    break;
                }
            }
            if (isAlready==false) {
                treats2.add(tmp_treats2.get(i));
            }
        }

        return treats2;
    }

    public ArrayList<AdverseEffectDrug> intersectAdverseEffectDrugs(ArrayList<AdverseEffectDrug> tmp_adverseeffects) {
        ArrayList<String> names1 = new ArrayList<>();
        ArrayList<String> names2 = new ArrayList<>();

        for (int i=0; i<this.adverseEffects.size(); i++) {
            names1.add(this.adverseEffects.get(i).getName());
        }
        for (int i=0; i<tmp_adverseeffects.size(); i++) {
            names2.add(tmp_adverseeffects.get(i).getName());
        }

        names1.retainAll(names2);
        names2.retainAll(names1);

        ArrayList<AdverseEffectDrug> adverseEffects2 = new ArrayList<>();
        ArrayList<AdverseEffectDrug> tmp_adverseeffects2 = new ArrayList<>();

        for (int i=0; i<this.adverseEffects.size(); i++) {
            for (int j=0; j<names1.size(); j++) {
                if (this.adverseEffects.get(i).getName().equals(names1.get(j))) {
                    adverseEffects2.add(this.adverseEffects.get(i));
                }
            }
        }

        for (int i=0; i<tmp_adverseeffects.size(); i++) {
            for (int j=0; j<names2.size(); j++) {
                if (tmp_adverseeffects.get(i).getName().equals(names2.get(j))) {
                    tmp_adverseeffects2.add(tmp_adverseeffects.get(i));
                }
            }
        }

        boolean isAlready = false;

        for (int i=0; i<tmp_adverseeffects2.size(); i++) {
            for (int j=0; j<adverseEffects2.size(); j++) {
                if (!adverseEffects2.get(j).getName().equals(tmp_adverseeffects2.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    for (String source:tmp_adverseeffects2.get(i).getSource()) {
                        if (!adverseEffects2.get(j).getSource().contains(source)) {
                            adverseEffects2.get(j).addSource(source);
                        }else{
                            adverseEffects2.get(j).addDisease(tmp_adverseeffects2.get(i).getDisease().get(0));
                        }
                    }
                    break;
                }
            }
            if (isAlready==false) {
                adverseEffects2.add(tmp_adverseeffects2.get(i));
            }
        }

        return adverseEffects2;
    }

    public ArrayList<Symptom> intersectSymptoms(ArrayList<Symptom> tmp_symptoms) {
        ArrayList<String> names1 = new ArrayList<>();
        ArrayList<String> names2 = new ArrayList<>();

        for (int i=0; i<this.symptoms.size(); i++) {
            names1.add(this.symptoms.get(i).getName());
        }
        for (int i=0; i<tmp_symptoms.size(); i++) {
            names2.add(tmp_symptoms.get(i).getName());
        }

        names1.retainAll(names2);
        names2.retainAll(names1);

        ArrayList<Symptom> symptoms2 = new ArrayList<>();
        ArrayList<Symptom> tmp_symptoms2 = new ArrayList<>();

        for (int i=0; i<this.symptoms.size(); i++) {
            for (int j=0; j<names1.size(); j++) {
                if (this.symptoms.get(i).getName().equals(names1.get(j))) {
                    symptoms2.add(this.symptoms.get(i));
                }
            }
        }

        for (int i=0; i<tmp_symptoms.size(); i++) {
            for (int j=0; j<names2.size(); j++) {
                if (tmp_symptoms.get(i).getName().equals(names2.get(j))) {
                    tmp_symptoms2.add(tmp_symptoms.get(i));
                }
            }
        }

        boolean isAlready = false;

        for (int i=0; i<tmp_symptoms2.size(); i++) {
            for (int j=0; j<symptoms2.size(); j++) {
                if (!symptoms2.get(j).getName().equals(tmp_symptoms2.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    for (String source:tmp_symptoms2.get(i).getSource()) {
                        if (!symptoms2.get(j).getSource().contains(source)) {
                            symptoms2.get(j).addSource(source);
                        }else{
                            symptoms2.get(j).addDisease(tmp_symptoms2.get(i).getDisease().get(0));
                        }
                    }
                    break;
                }
            }
            if (isAlready==false) {
                symptoms2.add(tmp_symptoms2.get(i));
            }
        }

        return symptoms2;
    }

    public void unify(Request_Results tmp) {
        ArrayList<TreatDrug> treats2 = this.unifyTreats(tmp.getTreats());
        ArrayList<AdverseEffectDrug> adverseEffects2 = this.unifyAdverseEffectDrugs(tmp.getAdverseEffects());
        ArrayList<Symptom> symptoms2 = this.unifySymptoms(tmp.getSymptoms());

        this.treats = treats2;
        this.adverseEffects = adverseEffects2;
        this.symptoms = symptoms2;
    }

    public ArrayList<TreatDrug> unifyTreats(ArrayList<TreatDrug> tmp_treats) {

        boolean isAlready = false;
        for (TreatDrug treatDrug: this.treats) {   // for all treats
            for (TreatDrug tmpTreatDrug:tmp_treats) {    // for all tmp_treats
                if(!tmpTreatDrug.getName().equals(treatDrug.getName())){  //if treat!=tmp_treat
                    isAlready = false;

                }else{  // if treat== tmp_treat
                    isAlready=true;
                    for(String source:treatDrug.getSource()){    // check sources
                        if(!tmpTreatDrug.getSource().contains(source)){  // if source isn't in sources
                            tmpTreatDrug.addSource(source);
                        }else{
                            tmpTreatDrug.addDisease(treatDrug.getDisease().get(0));
                        }
                    }
                    break;
                }
            }
            if (isAlready==false) {
                tmp_treats.add(treatDrug);
            }
        }
        return tmp_treats;
    }

    public ArrayList<AdverseEffectDrug> unifyAdverseEffectDrugs(ArrayList<AdverseEffectDrug> tmp_adverseeffects) {

        boolean isAlready = false;
        for (AdverseEffectDrug adverseEffectDrug: this.adverseEffects) {   // for all treats
            for (AdverseEffectDrug tmpAdverseEffectDrug:tmp_adverseeffects) {    // for all tmp_treats
                if(!tmpAdverseEffectDrug.getName().equals(adverseEffectDrug.getName())){  //if treat!=tmp_treat
                    isAlready = false;

                }else{  // if treat== tmp_treat
                    isAlready=true;
                    for(String source:adverseEffectDrug.getSource()){    // check sources
                        if(!tmpAdverseEffectDrug.getSource().contains(source)){  // if source isn't in sources
                            tmpAdverseEffectDrug.addSource(source);
                        }else{
                        tmpAdverseEffectDrug.addDisease(adverseEffectDrug.getDisease().get(0));
                    }
                    }
                    break;
                }
            }
            if (isAlready==false) {
                tmp_adverseeffects.add(adverseEffectDrug);
            }
        }
        return tmp_adverseeffects;
    }

    public ArrayList<Symptom> unifySymptoms(ArrayList<Symptom> tmp_symptoms) {
        boolean isAlready = false;
        for (int i=0; i<this.symptoms.size(); i++) {
            for (int j=0; j<tmp_symptoms.size(); j++) {
                if (!tmp_symptoms.get(j).getName().equals(symptoms.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    for (String source:this.symptoms.get(i).getSource()) {
                        if (!tmp_symptoms.get(j).getSource().contains(source)) {
                            tmp_symptoms.get(j).addSource(source);
                        }else {
                            tmp_symptoms.get(j).addDisease(this.symptoms.get(i).getDisease().get(0));
                        }
                    }
                    break;
                }
            }
            if (isAlready == false) {
                tmp_symptoms.add(symptoms.get(i));
            }
        }

        return tmp_symptoms;
    }

    public void printResults() {
        System.out.println("Results of your request :");
        System.out.println(" ");
        System.out.println("****** Treat Drugs *******");
        this.printTreatDrugs();
        System.out.println(" ");
        System.out.println("****** Adverse Effects Drugs *******");
        this.printAdverseEffectsDrugs();
        System.out.println(" ");
        System.out.println("****** Symptoms *******");
        this.printSymptoms();
        System.out.println(" ");
    }

    public void printTreatDrugs() {

        boolean hasTwoSources = false;
        ArrayList<String> sources = new ArrayList<String>();
        for (int i=0; i<treats.size(); i++) {
            if (treats.get(i).getSource().size()==1) {
                if (!sources.contains(treats.get(i).getFirstSource())) {
                    sources.add(treats.get(i).getFirstSource());
                }
            } else if (treats.get(i).getSource().size()==2) {
                hasTwoSources = true;
                if (!sources.contains(treats.get(i).getFirstSource())) {
                    sources.add(treats.get(i).getFirstSource());
                } else if (!sources.contains(treats.get(i).getSource().get(1))) {
                    sources.add(treats.get(i).getSource().get(1));
                }
            }
        }

        if (hasTwoSources) {
            System.out.println("Score : 2 // Sources : " + sources.get(0) + " & " + sources.get(1));
            for (int i = 0; i < treats.size(); i++) {
                if (treats.get(i).getSource().size() == 2) {
                    System.out.println("|"+"-"+"> " + treats.get(i).getName());
                }
            }
        }

        System.out.println(" ");
        for (int j=0; j<sources.size(); j++) {
            System.out.println("Score : 1 // --> Source : " + sources.get(j));
            for (int i = 0; i < treats.size(); i++) {
                if (treats.get(i).getSource().size()==1) {
                    if (treats.get(i).getFirstSource().equals(sources.get(j))) {
                        System.out.println("|"+"-"+"> " + treats.get(i).getName());
                    }
                }
            }
            System.out.println(" ");
        }
    }

    public void printAdverseEffectsDrugs() {
        boolean hasTwoSources = false;
        ArrayList<String> sources = new ArrayList<String>();

        for (int i=0; i<adverseEffects.size(); i++) {
            if (adverseEffects.get(i).getSource().size()==1) {
                if (!sources.contains(adverseEffects.get(i).getFirstSource())) {
                    sources.add(adverseEffects.get(i).getFirstSource());
                }
            } else if (adverseEffects.get(i).getSource().size()==2) {
                hasTwoSources = true;
                if (!sources.contains(adverseEffects.get(i).getFirstSource())) {
                    sources.add(adverseEffects.get(i).getFirstSource());
                } else if (!sources.contains(adverseEffects.get(i).getSource().get(1))) {
                    sources.add(adverseEffects.get(i).getSource().get(1));
                }
            }
        }

        if (hasTwoSources) {
            System.out.println("Score : 2 // Sources : " + sources.get(0) + " & " + sources.get(1));
            for (int i = 0; i < adverseEffects.size(); i++) {
                if (adverseEffects.get(i).getSource().size() == 2) {
                    System.out.println("|"+"-"+"> " + adverseEffects.get(i).getName());
                }
            }
        }

        System.out.println(" ");

        for (int j=0; j<sources.size(); j++) {
            System.out.println("Score : 1 // --> Source : " + sources.get(j));
            for (int i = 0; i < adverseEffects.size(); i++) {
                if (adverseEffects.get(i).getSource().size()==1) {
                    if (adverseEffects.get(i).getFirstSource().equals(sources.get(j))) {
                        System.out.println("|"+"-"+"> " + adverseEffects.get(i).getName());
                    }
                }
            }
            System.out.println(" ");
        }
    }

    public void printSymptoms() {
        boolean hasTwoSources = false;
        ArrayList<String> sources = new ArrayList<String>();

        for (int i=0; i<symptoms.size(); i++) {
            if (symptoms.get(i).getSource().size()==1) {
                if (!sources.contains(symptoms.get(i).getFirstSource())) {
                    sources.add(symptoms.get(i).getFirstSource());
                }
            } else if (symptoms.get(i).getSource().size()==2) {
                hasTwoSources = true;
                if (!sources.contains(symptoms.get(i).getFirstSource())) {
                    sources.add(symptoms.get(i).getFirstSource());
                } else if (!sources.contains(symptoms.get(i).getSource().get(1))) {
                    sources.add(symptoms.get(i).getSource().get(1));
                }
            }
        }

        if (hasTwoSources) {
            System.out.println("Score : 2 // Sources : " + sources.get(0) + " & " + sources.get(1));
            for (int i = 0; i < symptoms.size(); i++) {
                if (symptoms.get(i).getSource().size() == 2) {
                    System.out.println("|"+"-"+"> " + symptoms.get(i).getName());
                }
            }
        }

        System.out.println(" ");

        for (int j=0; j<sources.size(); j++) {
            System.out.println("Score : 1 // --> Source : " + sources.get(j));
            for (int i = 0; i < symptoms.size(); i++) {
                if (symptoms.get(i).getSource().size()==1) {
                    if (symptoms.get(i).getFirstSource().equals(sources.get(j))) {
                        System.out.println("|" + "-" + "> " + symptoms.get(i).getName());
                    }
                }
            }
            System.out.println(" ");
        }
    }
}

package eu.telecomnancy.gmd.Data_Searchers;

import eu.telecomnancy.gmd.OMIMAnalysis.searchIndexOMIM;
import eu.telecomnancy.gmd.ORPHA_DATA_Search.URLConnection;
import eu.telecomnancy.gmd.objects.Disease;
import eu.telecomnancy.gmd.objects.Symptom;
import eu.telecomnancy.gmd.objects.TreatDrug;
import org.apache.lucene.queryparser.classic.ParseException;

import java.io.IOException;
import java.util.ArrayList;

/*
* Class for the search of diseases which shares given symptoms
 */

public class DiseasesSharingSearcher {

    private String name;
    private ArrayList<Disease> ORPHAList;
    private ArrayList<Disease> OMIMList;
    private searchIndexOMIM omimDataBase;

    public DiseasesSharingSearcher(String name) throws Exception {
        // Initialization of the lists
        this.name = name;
        omimDataBase = new searchIndexOMIM();
        ORPHAList = new ArrayList<Disease>();
        OMIMList = new ArrayList<Disease>();

        // Get the lists
        ORPHAList = this.getOrphaSearch(this.name);
        OMIMList = this.getOMIMSearch(this.name);
    }

    /* Get the data from Orpha */
    public ArrayList<Disease> getOrphaSearch(String name) throws Exception {
        URLConnection couchdb = new URLConnection();
        if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
            couchdb.ConnectSymptoms(name);
        } else {
            couchdb.ConnectSymptoms2(name);
        }
        return couchdb.getDiseasesList();
    }

    /* Get the data from OMIM */
    public ArrayList<Disease> getOMIMSearch(String name) {
        ArrayList<Disease> diseaseList = null;
        try {
            if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
                diseaseList = omimDataBase.getDiseasesList(name);
            } else {
                diseaseList = omimDataBase.getDiseasesList2(name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (int i=0; i<diseaseList.size(); i++) {
            String name2 = diseaseList.get(i).getName();
            name2 = name2.substring(6);
            name2 = name2.substring(0, name2.length()-5);
            name2 = name2.trim();
            diseaseList.get(i).setName(name2);
        }
        return diseaseList;
    }

    /* Merge the both lists of Diseases */
    public ArrayList<Disease> getDiseasesList() {
        for (int i=0; i<ORPHAList.size(); i++) {
            ORPHAList.get(i).setName(ORPHAList.get(i).getName().trim().toLowerCase());
        }
        for (int i=0; i<OMIMList.size(); i++) {
            OMIMList.get(i).setName(OMIMList.get(i).getName().trim().toLowerCase());
        }

        boolean isAlready = false;
        ArrayList<Disease> diseaseGlobal = new ArrayList<Disease>();

        for (int i=0; i<ORPHAList.size(); i++) {
            for (int j=0; j<diseaseGlobal.size(); j++) {
                if (!diseaseGlobal.get(j).getName().equals(ORPHAList.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    break;
                }
            }
            if (isAlready==false) {
                diseaseGlobal.add(ORPHAList.get(i));
            }
            isAlready = false;
        }

        isAlready = false;

        for (int i=0; i<OMIMList.size(); i++) {
            for (int j=0; j<diseaseGlobal.size(); j++) {
                if (!diseaseGlobal.get(j).getName().equals(OMIMList.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    if (!diseaseGlobal.get(j).getSources().contains("OMIM")) {
                        diseaseGlobal.get(j).addSource(OMIMList.get(i).getFirstSource());
                    }
                    break;
                }
            }
            if (isAlready == false) {
                diseaseGlobal.add(OMIMList.get(i));
            }
        }

        return diseaseGlobal;
    }
}

package eu.telecomnancy.gmd.Data_Searchers;

/*
* Class for the search of a medicine information
 */

import eu.telecomnancy.gmd.SIDER2_Search.DatabaseConnection;
import eu.telecomnancy.gmd.XMLDataProcess.searchIndexXML;
import eu.telecomnancy.gmd.objects.Disease;
import eu.telecomnancy.gmd.objects.DrugInfo;
import org.apache.lucene.queryparser.classic.ParseException;

import java.io.IOException;
import java.util.ArrayList;

public class MedicineInformationSearcher {

    private String name;
    private ArrayList<DrugInfo> drugBankDrug;
    private ArrayList<Disease> sider2indication;
    private ArrayList<Disease> sider2toxicity;
    private searchIndexXML drugbankDataBase;
    private DrugInfo drug;
    private DatabaseConnection dataBase;
    private ArrayList<Disease> indications;
    private ArrayList<Disease> toxicity;


    public MedicineInformationSearcher(String name) {
        // Initialisation of the parameters
        this.name = name;
        drugbankDataBase = new searchIndexXML();
        dataBase= new DatabaseConnection();

        /* Get the different lists */
        sider2indication = this.SIDER2_Indication_Search(this.name);
        sider2toxicity = this.SIDER2_Toxicity_Search(this.name);
        drugBankDrug = this.getDrugBankSearch(this.name);

        /* Get the final lists merged and print them */
        this.indications = this.mergeIndications();
        this.toxicity = this.mergeToxicity();
        //this.printResults();
    }

    // Get the drug infos from the DrugBank database
    public ArrayList<DrugInfo> getDrugBankSearch(String name) {
        ArrayList<DrugInfo> drugInfos = null;
        try {
            if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
                drugInfos = drugbankDataBase.drugAnalysis(name);
            } else {
                drugInfos = drugbankDataBase.drugAnalysis2(name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return drugInfos;
    }

    /* Get the Sider2 indication */
    public ArrayList<Disease> SIDER2_Indication_Search(String name) {
        if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
            return dataBase.selectIndicationDisease(name);
        } else {
            return dataBase.selectIndicationDisease2(name);
        }
    }

    /* Get the Sider2 toxicity */
    public ArrayList<Disease> SIDER2_Toxicity_Search(String name) {
        if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
            return dataBase.selectToxicityDisease(name);
        } else {
            return dataBase.selectToxicityDisease2(name);
        }
    }

    // Merge the indications about the drug from the two databases
    public ArrayList<Disease> mergeIndications() {
        ArrayList<Disease> indications = new ArrayList<Disease>();
        boolean isAlready = false;

        for (int i=0; i<sider2indication.size(); i++) {
            sider2indication.get(i).setName(sider2indication.get(i).getName().trim().toLowerCase());
        }

        /* Add all Sider2 data without double */
        for (int i=0; i<sider2indication.size(); i++) {
            for (int j=0; j<indications.size(); j++) {
                if (!indications.get(j).getName().equals(sider2indication.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    break;
                }
            }
            if (isAlready==false) {
                indications.add(sider2indication.get(i));
            }
            isAlready = false;
        }

        for (int i=0; i<drugBankDrug.size(); i++) {
            for (int j=0; j<indications.size(); j++) {
                if (!indications.get(j).getName().equals(drugBankDrug.get(i).getIndication())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    if (!indications.get(j).getSources().contains("Drugbank")) {
                        indications.get(j).addSource("Drugbank");
                    }
                    break;
                }
            }
            if (isAlready == false) {
                if(drugBankDrug.get(i).getIndication().equals("")) {
                } else {
                    indications.add(new Disease(drugBankDrug.get(i).getIndication(), "Drugbank"));
                }
            }
        }

        return indications;
    }

    // Merge the toxicities about the drug from the two databases
    public ArrayList<Disease> mergeToxicity() {
        ArrayList<Disease> toxicity = new ArrayList<Disease>();
        boolean isAlready = false;

        for (int i=0; i<sider2toxicity.size(); i++) {
            sider2toxicity.get(i).setName(sider2toxicity.get(i).getName().trim().toLowerCase());
        }

        /* Add all Sider2 data without double */
        for (int i=0; i<sider2toxicity.size(); i++) {
            for (int j=0; j<toxicity.size(); j++) {
                if (!toxicity.get(j).getName().equals(sider2toxicity.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    break;
                }
            }
            if (isAlready==false) {
                toxicity.add(sider2toxicity.get(i));
            }
            isAlready = false;
        }

        for (int i=0; i<drugBankDrug.size(); i++) {
            for (int j=0; j<toxicity.size(); j++) {
                if (!toxicity.get(j).getName().equals(drugBankDrug.get(i).getToxicity())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    if (!toxicity.get(j).getSources().contains("Drugbank")) {
                        toxicity.get(j).addSource("Drugbank");
                    }
                    break;
                }
            }
            if (isAlready == false) {
                if(drugBankDrug.get(i).getToxicity().equals("")) {
                } else {
                    toxicity.add(new Disease(drugBankDrug.get(i).getToxicity(), "Drugbank"));
                }
            }
        }

        return toxicity;
    }

    public ArrayList<Disease> getIndications() {
        return this.indications;
    }

    public ArrayList<Disease> getToxicity() {
        return this.toxicity;
    }

    // Print the results
   /* public void printResults() {
        System.out.println("Searched Medicine : " + this.name);
        System.out.println("-------------------------");
        if (this.indications.size()!=0 && this.toxicity.size()!=0) {
            System.out.println("***** Indications : *****");
            if (this.indications.size()!=0) {

                for (String key : this.indications.keySet()) {
                    if (this.indications.get(key).size()==2) {
                        System.out.println(key);
                    }
                }

                for (String key : this.indications.keySet()) {
                    if (this.indications.get(key).size()==1) {
                        if (this.indications.get(key).get(0).equals("Drugbank")) {
                            System.out.println(key);
                        }
                    }
                }

                for (String key : this.indications.keySet()) {
                    if (this.indications.get(key).size()==1) {
                        if (this.indications.get(key).get(0).equals("sider2")) {
                            System.out.println(key);
                        }
                    }
                }

            } else {
                System.out.println("Nothing found.");
            }


            System.out.println("***** Toxicity : *****");
            if (this.toxicity.size()!=0) {

                for (String key : this.toxicity.keySet()) {
                    if (toxicity.get(key).size()==2) {
                        System.out.println(key);
                    }
                }

                for (String key : this.toxicity.keySet()) {
                    if (toxicity.get(key).size()==1) {
                        if (toxicity.get(key).get(0).equals("Drugbank")) {
                            System.out.println(key);
                        }
                    }
                }

                for (String key : this.toxicity.keySet()) {
                    if (toxicity.get(key).size()==1) {
                        if (toxicity.get(key).get(0).equals("sider2")) {
                            System.out.println(key);
                        }
                    }
                }
            } else {
                System.out.println("Nothing found.");
            }


            System.out.println(" ");
        } else {
            System.out.println("Nothing found.");
        }
    }*/
}

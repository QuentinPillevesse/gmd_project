package eu.telecomnancy.gmd.Data_Searchers;

import eu.telecomnancy.gmd.objects.AdverseEffectDrug;
import eu.telecomnancy.gmd.objects.Disease;
import eu.telecomnancy.gmd.objects.Symptom;
import eu.telecomnancy.gmd.objects.TreatDrug;

import java.util.ArrayList;

/*
Request about diseases sharing symptoms
 */

public class RequestSharing_Results {

    ArrayList<Disease> diseases;

    public RequestSharing_Results(ArrayList<Disease> diseases) {
        this.diseases = diseases;
    }

    public void intersect(RequestSharing_Results tmp_results) {
        ArrayList<Disease> diseases2 = this.intersectDiseases(tmp_results.getDiseases());

        this.diseases = diseases2;
    }

    public ArrayList<Disease> getDiseases() {
        return this.diseases;
    }

    public ArrayList<Disease> intersectDiseases(ArrayList<Disease> tmp_diseases) {

        ArrayList<String> names1 = new ArrayList<>();
        ArrayList<String> names2 = new ArrayList<>();

        for (int i=0; i<this.diseases.size(); i++) {
            names1.add(this.diseases.get(i).getName());
        }
        for (int i=0; i<tmp_diseases.size(); i++) {
            names2.add(tmp_diseases.get(i).getName());
        }

        names1.retainAll(names2);
        names2.retainAll(names1);

        ArrayList<Disease> diseases2 = new ArrayList<>();
        ArrayList<Disease> tmp_diseases2 = new ArrayList<>();

        for (int i=0; i<this.diseases.size(); i++) {
            for (int j=0; j<names1.size(); j++) {
                if (this.diseases.get(i).getName().equals(names1.get(j))) {
                    diseases2.add(this.diseases.get(i));
                }
            }
        }

        for (int i=0; i<tmp_diseases.size(); i++) {
            for (int j=0; j<names2.size(); j++) {
                if (tmp_diseases.get(i).getName().equals(names2.get(j))) {
                    tmp_diseases2.add(tmp_diseases.get(i));
                }
            }
        }

        boolean isAlready = false;

        for (int i=0; i<tmp_diseases2.size(); i++) {
            for (int j=0; j<diseases2.size(); j++) {
                if (!diseases2.get(j).getName().equals(tmp_diseases2.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    for (String source:tmp_diseases2.get(i).getSources()) {
                        if (!diseases2.get(j).getSources().contains(source)) {
                            diseases2.get(j).addSource(source);
                        }
                    }
                    break;
                }
            }
            if (isAlready==false) {
                diseases2.add(tmp_diseases2.get(i));
            }
        }

        return diseases2;
    }

    public void unify(RequestSharing_Results tmp_results) {
        ArrayList<Disease> diseases2 = this.unifyDiseases(tmp_results.getDiseases());

        this.diseases = diseases2;
    }

    public ArrayList<Disease> unifyDiseases(ArrayList<Disease> tmp_diseases) {

        boolean isAlready = false;
        for (Disease diseaseObj: this.diseases) {   // for all treats
            for (Disease tmpDiseaseObj:tmp_diseases) {    // for all tmp_treats
                if(!tmpDiseaseObj.getName().equals(diseaseObj.getName())){  //if treat!=tmp_treat
                    isAlready = false;

                }else{  // if treat== tmp_treat
                    isAlready=true;
                    for(String source:diseaseObj.getSources()){    // check sources
                        if(!tmpDiseaseObj.getSources().contains(source)){  // if source isn't in sources
                            tmpDiseaseObj.addSource(source);
                        }
                    }
                    break;
                }
            }
            if (isAlready==false) {
                tmp_diseases.add(diseaseObj);
            }
        }
        return tmp_diseases;
    }

    public void printResults() {
        System.out.println("Results of your request :");
        System.out.println(" ");
        System.out.println("****** Diseases *******");
        this.printDiseases();
        System.out.println(" ");
    }

    public void printDiseases() {

        boolean hasTwoSources = false;
        ArrayList<String> sources = new ArrayList<String>();
        for (int i=0; i<diseases.size(); i++) {
            if (diseases.get(i).getSources().size()==1) {
                if (!sources.contains(diseases.get(i).getFirstSource())) {
                    sources.add(diseases.get(i).getFirstSource());
                }
            } else if (diseases.get(i).getSources().size()==2) {
                hasTwoSources = true;
                if (!sources.contains(diseases.get(i).getFirstSource())) {
                    sources.add(diseases.get(i).getFirstSource());
                } else if (!sources.contains(diseases.get(i).getSources().get(1))) {
                    sources.add(diseases.get(i).getSources().get(1));
                }
            }
        }

        if (hasTwoSources) {
            System.out.println("Score : 2 // Sources : " + sources.get(0) + " & " + sources.get(1));
            for (int i = 0; i < diseases.size(); i++) {
                if (diseases.get(i).getSources().size() == 2) {
                    System.out.println("|"+"-"+"> " + diseases.get(i).getName());
                }
            }
        }

        System.out.println(" ");
        for (int j=0; j<sources.size(); j++) {
            System.out.println("Score : 1 // --> Source : " + sources.get(j));
            for (int i = 0; i < diseases.size(); i++) {
                if (diseases.get(i).getSources().size()==1) {
                    if (diseases.get(i).getFirstSource().equals(sources.get(j))) {
                        System.out.println("|"+"-"+"> " + diseases.get(i).getName());
                    }
                }
            }
            System.out.println(" ");
        }
    }
}

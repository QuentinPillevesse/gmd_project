package eu.telecomnancy.gmd.Data_Searchers;

import eu.telecomnancy.gmd.OMIMAnalysis.searchIndexOMIM;
import eu.telecomnancy.gmd.ORPHA_DATA_Search.URLConnection;
import eu.telecomnancy.gmd.SIDER2_Search.DatabaseConnection;
import eu.telecomnancy.gmd.XMLDataProcess.XMLAnalysis;
import eu.telecomnancy.gmd.XMLDataProcess.searchIndexXML;
import eu.telecomnancy.gmd.objects.AbstractDrug;
import eu.telecomnancy.gmd.objects.AdverseEffectDrug;
import eu.telecomnancy.gmd.objects.Symptom;
import eu.telecomnancy.gmd.objects.TreatDrug;
import org.apache.lucene.queryparser.classic.ParseException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Class for the search of disease information : medicament which cure it, which cause it and symptoms
 */

public class DiseaseInformationSearcher {

    private String name;
    private ArrayList<Symptom> ORPHA_DATA_Symptoms;
    private ArrayList<TreatDrug> XML_DATA_Medicine_Treat;
    private ArrayList<AdverseEffectDrug> XML_DATA_Medicine_Side_Effects;
    private ArrayList<TreatDrug> SIDER2_DATA_Medicine_Treat;
    private ArrayList<AdverseEffectDrug> SIDER2_DATA_Medicine_Side_Effects;
    private ArrayList<Symptom> OMIM_DATA_Symptoms;
    private DatabaseConnection dataBase;
    private XMLAnalysis analysis;
    private searchIndexXML drugbankDataBase;
    private searchIndexOMIM omimDataBase;

    public DiseaseInformationSearcher(String name) throws Exception {

        // Initialization of the different parameters
        this.name = name;
        ORPHA_DATA_Symptoms = new ArrayList<>();
        XML_DATA_Medicine_Treat = new ArrayList<>();
        XML_DATA_Medicine_Side_Effects = new ArrayList<>();
        SIDER2_DATA_Medicine_Treat = new ArrayList<>();
        SIDER2_DATA_Medicine_Side_Effects = new ArrayList<>();
        OMIM_DATA_Symptoms = new ArrayList<>();
        dataBase= new DatabaseConnection();
        this.drugbankDataBase=new searchIndexXML();
        this.omimDataBase=new searchIndexOMIM();

        // Call the different methods to get all datas
        ORPHA_DATA_Symptoms = this.ORPHA_DATA_Search(name);
        OMIM_DATA_Symptoms = this.OMIM_Search(name);
        SIDER2_DATA_Medicine_Treat = this.SIDER2_Treat_Search(name);
        SIDER2_DATA_Medicine_Side_Effects = this.SIDER2_Side_Effects_Search(name);
        XML_DATA_Medicine_Treat = this.XML_DATA_Treat_Search(name);
        XML_DATA_Medicine_Side_Effects = this.XML_DATA_Side_Effects_Search(name);
    }

    /* Get the symptoms from OMIM */
    public ArrayList<Symptom> OMIM_Search(String name) {
        ArrayList<Symptom> symptomList = null;
        try {
            if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
                symptomList = omimDataBase.getClinicalSignsList("START " + name + " STOP");
            } else {
                symptomList = omimDataBase.getClinicalSignsList2(name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return symptomList;
    }

    /* Get the symptoms from Orpha */
    public ArrayList<Symptom> ORPHA_DATA_Search(String name) throws Exception {
        URLConnection couchdb = new URLConnection();
        if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
            couchdb.ConnectDisease(name);
            couchdb.ConnectClinicalSigns(name);
        } else {
            couchdb.ConnectDisease2(name);
            couchdb.ConnectClinicalSigns2(name);
        }
        return couchdb.getClinicalSignsList();
    }

    /* Get the cure drugs from Sider2 */
    public ArrayList<TreatDrug> SIDER2_Treat_Search(String name) {
        if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
            return dataBase.selectDrug(name);
        } else {
            return dataBase.selectDrug2(name);
        }
    }

    /* Get the adverse effect drugs from Sider2 */
    public ArrayList<AdverseEffectDrug> SIDER2_Side_Effects_Search(String name) {
        if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
            return dataBase.selectDrugAdverse(name);
        } else {
            return dataBase.selectDrugAdverse2(name);
        }
    }

    /* Get the cure drugs from Drugbank */
    public ArrayList<TreatDrug> XML_DATA_Treat_Search(String name) {
        ArrayList<TreatDrug> treatDrugList = null;
        try {
            if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
                treatDrugList = drugbankDataBase.cureAnalysis(name);
            } else {
                treatDrugList = drugbankDataBase.cureAnalysis2(name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return treatDrugList;
    }

    /* Get the adverse effect drugs from Drugbank */
    public ArrayList<AdverseEffectDrug> XML_DATA_Side_Effects_Search(String name) {
        ArrayList<AdverseEffectDrug> AdverseEffectDrugList = null;
        try {
            if (name.indexOf('*')==-1 && name.indexOf('?')==-1) {
                AdverseEffectDrugList = drugbankDataBase.adverseEffectsAnalysis(name);
            } else {
                AdverseEffectDrugList = drugbankDataBase.adverseEffectsAnalysis2(name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return AdverseEffectDrugList;
    }

    /* Merge the both lists of cure drugs */
    public ArrayList<TreatDrug> getTreatList() {

        /* All names in lower case */
        for (int i=0; i<SIDER2_DATA_Medicine_Treat.size(); i++) {
            SIDER2_DATA_Medicine_Treat.get(i).setName(SIDER2_DATA_Medicine_Treat.get(i).getName().trim().toLowerCase());
        }
        for (int i=0; i<XML_DATA_Medicine_Treat.size(); i++) {
            XML_DATA_Medicine_Treat.get(i).setName(XML_DATA_Medicine_Treat.get(i).getName().trim().toLowerCase());
        }

        boolean isAlready = false;
        ArrayList<TreatDrug> MedicineTreats = new ArrayList<TreatDrug>();

        /* Add all Sider2 data without double */
        for (int i=0; i<SIDER2_DATA_Medicine_Treat.size(); i++) {
            for (int j=0; j<MedicineTreats.size(); j++) {
                if (!MedicineTreats.get(j).getName().equals(SIDER2_DATA_Medicine_Treat.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    break;
                }
            }
            if (isAlready==false) {
                MedicineTreats.add(SIDER2_DATA_Medicine_Treat.get(i));
            }
            isAlready = false;
        }

        isAlready = false;

        /* Add all Drugbank data without double */
        for (int i=0; i<XML_DATA_Medicine_Treat.size(); i++) {
            for (int j=0; j<MedicineTreats.size(); j++) {
                if (!MedicineTreats.get(j).getName().equals(XML_DATA_Medicine_Treat.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    if (!MedicineTreats.get(j).getSource().contains("Drugbank")) {
                        MedicineTreats.get(j).addSource(XML_DATA_Medicine_Treat.get(i).getFirstSource());
                    }
                    break;
                }
            }
            if (isAlready == false) {
                MedicineTreats.add(XML_DATA_Medicine_Treat.get(i));
            }
        }

        return MedicineTreats;
    }

    /* Merge the both lists of adverse effects drugs */
    public ArrayList<AdverseEffectDrug> getAdverseEffectList() {
        ArrayList<AdverseEffectDrug> AdverseEffectDrugs = new ArrayList<AdverseEffectDrug>();
        boolean isAlready = false;

        for (int i=0; i<SIDER2_DATA_Medicine_Side_Effects.size(); i++) {
            SIDER2_DATA_Medicine_Side_Effects.get(i).setName(SIDER2_DATA_Medicine_Side_Effects.get(i).getName().trim().toLowerCase());
        }
        for (int i=0; i<XML_DATA_Medicine_Side_Effects.size(); i++) {
            XML_DATA_Medicine_Side_Effects.get(i).setName(XML_DATA_Medicine_Side_Effects.get(i).getName().trim().toLowerCase());
        }

        for (int i=0; i<SIDER2_DATA_Medicine_Side_Effects.size(); i++) {
            for (int j=0; j<AdverseEffectDrugs.size(); j++) {
                if (!AdverseEffectDrugs.get(j).getName().equals(SIDER2_DATA_Medicine_Side_Effects.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    break;
                }
            }
            if (isAlready==false) {
                AdverseEffectDrugs.add(SIDER2_DATA_Medicine_Side_Effects.get(i));
            }
            isAlready = false;
        }

        isAlready = false;

        for (int i=0; i<XML_DATA_Medicine_Side_Effects.size(); i++) {
            for (int j=0; j<AdverseEffectDrugs.size(); j++) {
                if (!AdverseEffectDrugs.get(j).getName().equals(XML_DATA_Medicine_Side_Effects.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    if (!AdverseEffectDrugs.get(j).getSource().contains("Drugbank")) {
                        AdverseEffectDrugs.get(j).addSource(XML_DATA_Medicine_Side_Effects.get(i).getFirstSource());
                    }
                    break;
                }
            }
            if (isAlready == false) {
                AdverseEffectDrugs.add(XML_DATA_Medicine_Side_Effects.get(i));
            }
        }

        return AdverseEffectDrugs;
    }

    /* Merge the both lists of symptoms */
    public ArrayList<Symptom> getSymptomsList() {
        ArrayList<Symptom> symptomsList = new ArrayList<Symptom>();
        boolean isAlready = false;

        for (int i=0; i<ORPHA_DATA_Symptoms.size(); i++) {
            ORPHA_DATA_Symptoms.get(i).setName(ORPHA_DATA_Symptoms.get(i).getName().trim().toLowerCase());
        }
        for (int i=0; i<OMIM_DATA_Symptoms.size(); i++) {
            OMIM_DATA_Symptoms.get(i).setName(OMIM_DATA_Symptoms.get(i).getName().trim().toLowerCase());
        }

        for (int i=0; i<ORPHA_DATA_Symptoms.size(); i++) {
            for (int j=0; j<symptomsList.size(); j++) {
                if (!symptomsList.get(j).getName().equals(ORPHA_DATA_Symptoms.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    break;
                }
            }
            if (isAlready==false) {
                symptomsList.add(ORPHA_DATA_Symptoms.get(i));
            }
            isAlready = false;
        }

        isAlready = false;

        for (int i=0; i<OMIM_DATA_Symptoms.size(); i++) {
            for (int j=0; j<symptomsList.size(); j++) {
                if (!symptomsList.get(j).getName().equals(OMIM_DATA_Symptoms.get(i).getName())) {
                    isAlready = false;
                } else {
                    isAlready = true;
                    if (!symptomsList.get(j).getSource().contains("OMIM")) {
                        symptomsList.get(j).addSource(OMIM_DATA_Symptoms.get(i).getFirstSource());
                    }
                    break;
                }
            }
            if (isAlready == false) {
                symptomsList.add(OMIM_DATA_Symptoms.get(i));
            }
        }

        return symptomsList;
    }

    /* Print the lists */
    public void printLists() {
        System.out.println("****************MySQL*******************");
        System.out.println("****Treats****");
        for(AbstractDrug drug:SIDER2_DATA_Medicine_Treat){
            System.out.println(drug.getName()+"    "+drug.getSource());
        }

        System.out.println("****Adverse_effects****");
        for(AbstractDrug drug:SIDER2_DATA_Medicine_Side_Effects){
            System.out.println(drug.getName()+"   "+drug.getSource());
        }

        System.out.println("****************XML*******************");
        System.out.println("****Treats****");
        for(AbstractDrug drug:XML_DATA_Medicine_Treat){
            System.out.println(drug.getName()+"   "+drug.getSource());
        }


        System.out.println("****Adverse_effects****");
        for(AbstractDrug drug:XML_DATA_Medicine_Side_Effects){
            System.out.println(drug.getName()+"   "+drug.getSource());
        }

        System.out.println("****************OMIM******************");
        System.out.println("Symptômes de la maladie : " + name);
        for (Symptom symptom:OMIM_DATA_Symptoms) {
            System.out.println(symptom.getName()+"   "+symptom.getSource());
        }

        System.out.println("****************CouchDB******************");
        System.out.println("Symptômes de la maladie : " + name);
        for (Symptom symptom:ORPHA_DATA_Symptoms) {
            System.out.println(symptom.getName()+"   "+symptom.getSource());
        }

    }
}

package eu.telecomnancy.gmd.Data_Searchers;

import eu.telecomnancy.gmd.objects.Disease;

import java.util.ArrayList;
import java.util.HashMap;


public class RequestMedicine_Results {

    private ArrayList<Disease> indications;
    private ArrayList<Disease> toxicity;
    private String name;

    public RequestMedicine_Results(ArrayList<Disease> indications, ArrayList<Disease> toxicity, String name) {
        this.indications = indications;
        this.toxicity = toxicity;
        this.name = name;
    }

    public void printResults() {
        System.out.println("Searched Medicine : " + this.name);
        System.out.println("-------------------------");
            System.out.println("***** Indications : *****");

            if (this.indications.size()!=0) {
                this.printIndicationsResults();
            } else {
                System.out.println("Nothing found.");
            }

            System.out.println("***** Toxicity : *****");

            if (this.toxicity.size()!=0) {
                this.printToxicityResults();
            } else {
                System.out.println("Nothing found.");
            }

            System.out.println(" ");
    }

    public void printIndicationsResults() {
        boolean hasTwoSources = false;
        ArrayList<String> sources = new ArrayList<String>();

        for (int i = 0; i < indications.size(); i++) {
            if (indications.get(i).getSources().size() == 1) {
                if (!sources.contains(indications.get(i).getFirstSource())) {
                    sources.add(indications.get(i).getFirstSource());
                }
            } else if (indications.get(i).getSources().size() == 2) {
                hasTwoSources = true;
                if (!sources.contains(indications.get(i).getFirstSource())) {
                    sources.add(indications.get(i).getFirstSource());
                } else if (!sources.contains(indications.get(i).getSources().get(1))) {
                    sources.add(indications.get(i).getSources().get(1));
                }
            }
        }

        if (hasTwoSources) {
            System.out.println("Score : 2 // Sources : " + sources.get(0) + " & " + sources.get(1));
            for (int i = 0; i < indications.size(); i++) {
                if (indications.get(i).getSources().size() == 2) {
                    System.out.println("|" + "-" + "> " + indications.get(i).getName());
                }
            }
        }

        System.out.println(" ");
        for (int j = 0; j < sources.size(); j++) {
            System.out.println("Score : 1 // --> Source : " + sources.get(j));
            for (int i = 0; i < indications.size(); i++) {
                if (indications.get(i).getSources().size() == 1) {
                    if (indications.get(i).getFirstSource().equals(sources.get(j))) {
                        System.out.println("|" + "-" + "> " + indications.get(i).getName());
                    }
                }
            }
            System.out.println(" ");
        }
    }

    public void printToxicityResults() {
        boolean hasTwoSources = false;
        ArrayList<String> sources = new ArrayList<String>();

        for (int i = 0; i < toxicity.size(); i++) {
            if (toxicity.get(i).getSources().size() == 1) {
                if (!sources.contains(toxicity.get(i).getFirstSource())) {
                    sources.add(toxicity.get(i).getFirstSource());
                }
            } else if (toxicity.get(i).getSources().size() == 2) {
                hasTwoSources = true;
                if (!sources.contains(toxicity.get(i).getFirstSource())) {
                    sources.add(toxicity.get(i).getFirstSource());
                } else if (!sources.contains(toxicity.get(i).getSources().get(1))) {
                    sources.add(toxicity.get(i).getSources().get(1));
                }
            }
        }

        if (hasTwoSources) {
            System.out.println("Score : 2 // Sources : " + sources.get(0) + " & " + sources.get(1));
            for (int i = 0; i < toxicity.size(); i++) {
                if (toxicity.get(i).getSources().size() == 2) {
                    System.out.println("|" + "-" + "> " + toxicity.get(i).getName());
                }
            }
        }

        System.out.println(" ");
        for (int j = 0; j < sources.size(); j++) {
            System.out.println("Score : 1 // --> Source : " + sources.get(j));
            for (int i = 0; i < toxicity.size(); i++) {
                if (toxicity.get(i).getSources().size() == 1) {
                    if (toxicity.get(i).getFirstSource().equals(sources.get(j))) {
                        System.out.println("|" + "-" + "> " + toxicity.get(i).getName());
                    }
                }
            }
            System.out.println(" ");
        }
    }

    public ArrayList<Disease> getIndications() {
        return indications;
    }

    public ArrayList<Disease> getToxicity() {
        return toxicity;
    }
}

package eu.telecomnancy.gmd.Data_Searchers;

/* Parse the requests with the AND priority */

public class ParserANDOR {

    /* Request for disease sharing symptoms */
    public RequestSharing_Results requestSharingSearch(String request) throws Exception {

        DiseasesSharingSearcher search = new DiseasesSharingSearcher(request);
        RequestSharing_Results results = new RequestSharing_Results(search.getDiseasesList());
        return results;

    }

    /* Request for disease information */
    public Request_Results requestSearch(String request) throws Exception {

        DiseaseInformationSearcher search = new DiseaseInformationSearcher(request);
        Request_Results results = new Request_Results(search.getTreatList(), search.getAdverseEffectList(), search.getSymptomsList());
        return results;

    }

    /* Request for medicine information */
    public RequestMedicine_Results requestMedicineSearch(String request) throws Exception {
        MedicineInformationSearcher search = new MedicineInformationSearcher(request);
        RequestMedicine_Results results = new RequestMedicine_Results(search.getIndications(), search.getToxicity(), request);
        return results;
    }

    /* Request of disease information parser on AND */
    public Request_Results requestANDParser(String request) throws Exception {
        String[] requests = request.split("&&");
        Request_Results r = null;
        r = this.requestSearch(requests[0].trim());
        for (int i = 1; i < requests.length; i++) {
            requests[i] = requests[i].trim();
            Request_Results tmp = this.requestSearch(requests[i]);
            // Intersect the results
            r.intersect(tmp);
        }
        return r;
    }

    /* Request of disease information parser on OR */
    public Request_Results requestORParser(String request) throws Exception {
        String[] requests = request.split("\\|\\|");
        Request_Results r = null;
        r = this.requestANDParser(requests[0].trim());
        for (int i = 1; i < requests.length; i++) {
            requests[i] = requests[i].trim();
            Request_Results tmp = this.requestANDParser(requests[i]);
            // Unify the results
            r.unify(tmp);
        }
        return r;
    }

    /* Request of disease sharing symptoms parser on AND */
    public RequestSharing_Results requestANDParser2(String request) throws Exception {
        String[] requests = request.split("&&");
        RequestSharing_Results r = null;
        r = this.requestSharingSearch(requests[0].trim());
        for (int i = 1; i < requests.length; i++) {
            requests[i] = requests[i].trim();
            RequestSharing_Results tmp = this.requestSharingSearch(requests[i]);
            // Intersect the results
            r.intersect(tmp);
        }
        return r;
    }

    /* Request of disease sharing symptoms parser on OR */
    public RequestSharing_Results requestORParser2(String request) throws Exception {
        String[] requests = request.split("\\|\\|");
        RequestSharing_Results r = null;
        r = this.requestANDParser2(requests[0].trim());
        for (int i = 1; i < requests.length; i++) {
            requests[i] = requests[i].trim();
            RequestSharing_Results tmp = this.requestANDParser2(requests[i]);
            // Unify the results
            r.unify(tmp);
        }
        return r;
    }

}

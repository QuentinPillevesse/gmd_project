package eu.telecomnancy.gmd;

import eu.telecomnancy.gmd.Data_Searchers.*;

import java.util.Scanner;


public class ConsoleApp {

    Request_Results r;
    RequestSharing_Results r2;
    RequestMedicine_Results r3;
    Scanner reader = new Scanner(System.in);
    ParserANDOR parserANDOR = new ParserANDOR();

    public ConsoleApp() throws Exception {

        // Enter the request and then will call the request parser methods
        System.out.println("******* Console App *******");
        System.out.println("Choose the criteria of your request :");
        System.out.println("1. Diseases");
        System.out.println("2. Symptoms");
        System.out.println("3. Medicine");
        System.out.println("4. Exit");

        int choice2;
        String choice;
        choice2 = reader.nextInt();
        while (choice2 != 4) {
            if (choice2 != 1 && choice2 != 2 && choice2 != 3) {
                System.out.println("Wrong input, try again !");
                choice2 = reader.nextInt();
                reader.nextLine();
            } else {
                reader.nextLine();
                switch (choice2) {
                    case 1:
                        this.ConsoleDiseaseApp();
                        break;
                    case 2:
                        this.ConsoleSymptomApp();
                        break;
                    case 3:
                        this.ConsoleMedicineApp();
                        break;
                }
                System.out.println(" ");
                System.out.println("Choose the criteria of your request :");
                System.out.println("1. Diseases");
                System.out.println("2. Symptoms");
                System.out.println("3. Medicine");
                System.out.println("4. Exit");

                choice2 = reader.nextInt();
            }

        }

    }

    private void ConsoleDiseaseApp() throws Exception {
        System.out.println("Please enter a disease (q for quit) :");
        //Scanner reader = new Scanner(System.in);
        String request = reader.nextLine();

        while(!request.equals("q")){
            r = parserANDOR.requestORParser(request);
            System.out.println(" ");
            r.printResults();

            System.out.println("Please enter a disease (q for quit) :");
            request = reader.nextLine();
        }
        //reader.close();
    }

    private void ConsoleSymptomApp() throws Exception {
        System.out.println("Please enter a symptom (q for quit) :");
        String request = reader.nextLine();

        while(!request.equals("q")){
            r2 = parserANDOR.requestORParser2(request);
            System.out.println(" ");
            r2.printResults();

            System.out.println("Please enter a symptom (q for quit) :");
            request = reader.nextLine();
        }
    }

    private void ConsoleMedicineApp() throws Exception {
        System.out.println("Please enter a medicine (q for quit) :");
        String request = reader.nextLine();

        while(!request.equals("q")){
            r3 = parserANDOR.requestMedicineSearch(request);
            System.out.println(" ");
            r3.printResults();

            System.out.println("Please enter a medicine (q for quit) :");
            request = reader.nextLine();
        }
    }


}
package eu.telecomnancy.gmd.objects;

import java.util.ArrayList;

public class Symptom {
    private String name;
    private ArrayList<String> source;
    private ArrayList<String> disease=new ArrayList<>();;

    public Symptom(String name,String source){
        this.name=name;
        this.source = new ArrayList<String>();
        this.source.add(source);
    }

    public Symptom(String name,String source,String disease){
        this.name=name;
        this.source = new ArrayList<String>();
        this.source.add(source);
        this.disease.add(disease);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSource(ArrayList<String> source) {
        this.source = source;
    }

    public String getName(){
        return this.name;
    }

    public ArrayList<String> getSource(){
        return this.source;
    }

    public void addSource(String source) {
        this.source.add(source);
    }

    public String getFirstSource() {
        return this.source.get(0);
    }

    public ArrayList<String> getDisease(){
        return this.disease;
    }

    public void addDisease(String disease){
        this.disease.add(disease);
    }
}


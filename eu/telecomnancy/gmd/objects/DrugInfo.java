package eu.telecomnancy.gmd.objects;


public class DrugInfo {

    private String name;
    private String indication;
    private String toxicity;

    public DrugInfo(String name, String indication, String toxicity) {
        this.name = name;
        this.indication = indication;
        this.toxicity = toxicity;
    }

    public String getName() {
        return this.name;
    }

    public String getIndication() {
        return this.indication;
    }

    public String getToxicity() {
        return this.toxicity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public void setToxicity(String toxicity) {
        this.toxicity= toxicity;
    }

}

package eu.telecomnancy.gmd.objects;


import java.util.ArrayList;

public class AdverseEffectDrug extends AbstractDrug {
    public AdverseEffectDrug(String name,String source) {
        this.name=name;
        this.source = new ArrayList<String>();
        this.addSource(source);
    }

    public AdverseEffectDrug(String name,String source,String disease) {
        this.name=name;
        this.source = new ArrayList<String>();
        this.addSource(source);
        this.disease.add(disease);
    }
}

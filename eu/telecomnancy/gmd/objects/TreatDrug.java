package eu.telecomnancy.gmd.objects;


import java.util.ArrayList;

public class TreatDrug extends AbstractDrug {

    public TreatDrug(String name,String source) {
        this.name=name;
        this.source = new ArrayList<String>();
        this.addSource(source);

    }

    public TreatDrug(String name,String source,String disease) {
        this.name=name;
        this.source = new ArrayList<String>();
        this.addSource(source);
        this.disease.add(disease);

    }
}

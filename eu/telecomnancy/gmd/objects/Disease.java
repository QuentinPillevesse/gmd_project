package eu.telecomnancy.gmd.objects;

import java.util.ArrayList;

/**
 * Created by pipou on 26/04/16.
 */
public class Disease {

    private String name;
    private ArrayList<String> sources;

    public Disease(String name, String source) {
        this.name = name;
        this.sources = new ArrayList<String>();
        this.sources.add(source);
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<String> getSources() {
        return this.sources;
    }

    public void addSource(String source) {
        this.sources.add(source);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstSource() {
        return this.sources.get(0);
    }
}

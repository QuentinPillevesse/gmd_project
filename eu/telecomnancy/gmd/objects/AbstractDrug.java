package eu.telecomnancy.gmd.objects;


import java.util.ArrayList;

public abstract class  AbstractDrug {
    protected String name;
    protected ArrayList<String> source;
    protected ArrayList<String> disease=new ArrayList<>();


    public void setName(String name) {
        this.name = name;
    }

    public void setSource(ArrayList<String> source) {
        this.source = source;
    }

    public String getName(){
        return this.name;
    }

    public String getFirstSource(){
        return this.source.get(0);
    }

    public void addSource(String source) {
        this.source.add(source);
    }

    public ArrayList<String> getSource() {
        return this.source;
    }

    public ArrayList<String> getDisease() {
        return disease;
    }

    public void setDisease(ArrayList<String> disease) {
        this.disease = disease;
    }

    public void addDisease(String disease){
        this.disease.add(disease);
    }

}


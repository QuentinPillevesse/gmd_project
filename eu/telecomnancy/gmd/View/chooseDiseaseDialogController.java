package eu.telecomnancy.gmd.View;


import eu.telecomnancy.gmd.Data_Searchers.Biostar45366;
import eu.telecomnancy.gmd.Data_Searchers.SynonymsSearcher;
import eu.telecomnancy.gmd.GUIApp;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class chooseDiseaseDialogController {


    @FXML
    private TextField diseaseNameField;

    @FXML
    private CheckBox checkBoxSynonym;

    @FXML
    private CheckBox checkBoxFamilly;



    private Stage dialogStage;
    private String diseaseName;
    private GUIApp mainApp;



    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;

    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(GUIApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
    }






    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleSearch() {
        this.diseaseName=diseaseNameField.getText();
        if(checkBoxSynonym.isSelected()){
            SynonymsSearcher synonymsSearcher=new SynonymsSearcher();
            mainApp.showDiseaseView(synonymsSearcher.searchSynonyms(this.diseaseName));

        }else if(checkBoxFamilly.isSelected()){
            Biostar45366 parser= null;
            try {
                parser = Biostar45366.parse(new File("doid.obo"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            mainApp.showDiseaseView(parser.request(parser.getDisease(this.diseaseName)));
        }else {
            mainApp.showDiseaseView(this.diseaseName);
        }
        dialogStage.close();
    }


    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }



}

package eu.telecomnancy.gmd.View;

import eu.telecomnancy.gmd.Data_Searchers.ParserANDOR;
import eu.telecomnancy.gmd.Data_Searchers.RequestMedicine_Results;
import eu.telecomnancy.gmd.GUIApp;
import eu.telecomnancy.gmd.objects.Disease;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;

public class medicineViewController {

    @FXML
    private Label medicineLabel;


    @FXML
    private  ListView listIndicationDrugBank_Sider2;
    @FXML
    private  ListView listIndicationDrugBank;
    @FXML
    private  ListView listIndicationSider2;

    @FXML
    private  ListView listToxicityDrugBank_Sider2;
    @FXML
    private  ListView listToxicityDrugBank;
    @FXML
    private  ListView listToxicitySider2;
    

    @FXML
    private  TitledPane paneIndicationDrugBank_Sider2;
    @FXML
    private  TitledPane paneIndicationDrugBank;
    @FXML
    private  TitledPane paneIndicationSider2;

    @FXML
    private  TitledPane paneToxicityDrugBank_Sider2;
    @FXML
    private  TitledPane paneToxicityDrugBank;
    @FXML
    private  TitledPane paneToxicitySider2;




    // Reference to the main application.
    private GUIApp mainApp;

    private String medicine;
    private ParserANDOR parserANDOR=new ParserANDOR();
    private RequestMedicine_Results request_results;


    private ObservableList<String> indicationListDrugBank_Sider2=FXCollections.observableArrayList ();
    private ObservableList<String> indicationListDrugBank=FXCollections.observableArrayList ();
    private ObservableList<String> indicationListSider2=FXCollections.observableArrayList ();

    private ObservableList<String> toxicityListDrugBank_Sider2=FXCollections.observableArrayList ();
    private ObservableList<String> toxicityListDrugBank=FXCollections.observableArrayList ();
    private ObservableList<String> toxicityListSider2=FXCollections.observableArrayList ();


    /*
         * The constructor.
         * The constructor is called before the initialize() method.
         */
    public medicineViewController() {

    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(GUIApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table

    }
    public void setMedicine(String medicine){
        this.medicine=medicine;
        medicineLabel.setText(this.medicine);
    }

    public void getData() throws Exception {
        request_results=parserANDOR.requestMedicineSearch(this.medicine);


        for (Disease disease:request_results.getIndications()){
            if(disease.getSources().size()==2){
                indicationListDrugBank_Sider2.add(disease.getName());
            }else if(disease.getSources().get(0).equals("Drugbank")){
                indicationListDrugBank.add(disease.getName());
            }else if(disease.getSources().get(0).equals("sider2")){
                indicationListSider2.add(disease.getName());
            }
        }

        for (Disease disease:request_results.getToxicity()){
            if(disease.getSources().size()==2){
                toxicityListDrugBank_Sider2.add(disease.getName());
            }else if(disease.getSources().get(0).equals("Drugbank")){
                toxicityListDrugBank.add(disease.getName());
            }else if(disease.getSources().get(0).equals("sider2")){
                toxicityListSider2.add(disease.getName());
            }
        }




        listIndicationDrugBank_Sider2.setItems(indicationListDrugBank_Sider2);
        paneIndicationDrugBank_Sider2.setText("DrugBank & Sider2 ("+indicationListDrugBank_Sider2.size()+")");
        listIndicationDrugBank.setItems(indicationListDrugBank);
        paneIndicationDrugBank.setText("DrugBank ("+indicationListDrugBank.size()+")");
        listIndicationSider2.setItems(indicationListSider2);
        paneIndicationSider2.setText("Sider2 ("+indicationListSider2.size()+")");

        listToxicityDrugBank_Sider2.setItems(toxicityListDrugBank_Sider2);
        paneToxicityDrugBank_Sider2.setText("DrugBank & Sider2 ("+toxicityListDrugBank_Sider2.size()+")");
        listToxicityDrugBank.setItems(toxicityListDrugBank);
        paneToxicityDrugBank.setText("DrugBank ("+toxicityListDrugBank.size()+")");
        listToxicitySider2.setItems(toxicityListSider2);
        paneToxicitySider2.setText("Sider2 ("+toxicityListSider2.size()+")");


    }

    @FXML
    private void handleChoiceWindows() {

        mainApp.showChoices();
    }


    @FXML
    private void handleMedicine() {
        mainApp.showChooseMedicineDialog();

    }
}

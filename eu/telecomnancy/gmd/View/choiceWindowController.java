package eu.telecomnancy.gmd.View;

import javafx.fxml.FXML;
import eu.telecomnancy.gmd.GUIApp;

public class choiceWindowController {


    // Reference to the main application.
    private GUIApp mainApp;


    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public choiceWindowController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(GUIApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table

    }

    @FXML
    private void handleChoiceWindows() {

        mainApp.showChoices();
    }


    @FXML
    private void handleDisease() {
        mainApp.showChooseDiseaseDialog();

    }

    @FXML
    private void handleSymptom() {
        mainApp.showChooseSymptomDialog();

    }


    @FXML
    private void handleMedicine() {
        mainApp.showChooseMedicineDialog();

    }
}

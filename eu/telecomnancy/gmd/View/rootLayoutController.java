package eu.telecomnancy.gmd.View;


import eu.telecomnancy.gmd.GUIApp;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;

public class rootLayoutController {


    // Reference to the main application.
    private GUIApp mainApp;
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(GUIApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table

    }
    /**
     * Closes the application.
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }


    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("App");
        alert.setHeaderText("About");
        alert.setContentText("Author: Loris,Quentin,Marie");

        alert.showAndWait();
    }

    @FXML
    private void handleMenu() {
        mainApp.showChoices();
    }
}

package eu.telecomnancy.gmd.View;


import eu.telecomnancy.gmd.Data_Searchers.ParserANDOR;
import eu.telecomnancy.gmd.Data_Searchers.RequestSharing_Results;
import eu.telecomnancy.gmd.Data_Searchers.Request_Results;
import eu.telecomnancy.gmd.GUIApp;
import eu.telecomnancy.gmd.objects.Disease;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;

public class symptomViewController {

    @FXML
    private Label symptomLabel;
    @FXML
    private ListView listOMIM_ORPHA;
    @FXML
    private  ListView listOMIM;
    @FXML
    private  ListView listORPHA;


    @FXML
    private TitledPane paneOMIM_ORPHA;
    @FXML
    private  TitledPane paneOMIM;
    @FXML
    private  TitledPane paneORPHA;






    // Reference to the main application.
    private GUIApp mainApp;

    private String symptom;
    private ParserANDOR parserANDOR=new ParserANDOR();
    private Request_Results request_results;
    private ObservableList<String> diseaseListOMIM_ORPHA= FXCollections.observableArrayList ();
    private ObservableList<String> diseaseListOMIM=FXCollections.observableArrayList ();
    private ObservableList<String> diseaseListORPHA=FXCollections.observableArrayList ();




    /*
         * The constructor.
         * The constructor is called before the initialize() method.
         */
    public symptomViewController() {

    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(GUIApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table

    }
    public void setSymptom(String symptom){
        this.symptom=symptom;
        symptomLabel.setText(this.symptom);
    }

    public void getData() throws Exception {
        RequestSharing_Results requestSharing_results =parserANDOR.requestORParser2(this.symptom);

        for (Disease disease:requestSharing_results.getDiseases()){
            if(disease.getSources().size()==2){
                diseaseListOMIM_ORPHA.add(disease.getName());
            }else if(disease.getSources().get(0).equals("orpha")){
                diseaseListORPHA.add(disease.getName());
            }else if(disease.getSources().get(0).equals("OMIM")){
                diseaseListOMIM.add(disease.getName());
            }

        }


        listOMIM_ORPHA.setItems(diseaseListOMIM_ORPHA);
        paneOMIM_ORPHA.setText("OMIM & Orpha ("+diseaseListOMIM_ORPHA.size()+")");
        listORPHA.setItems(diseaseListORPHA);
        paneORPHA.setText("Orpha ("+diseaseListORPHA.size()+")");
        listOMIM.setItems(diseaseListOMIM);
        paneOMIM.setText("OMIM ("+diseaseListOMIM.size()+")");




    }

    @FXML
    private void handleChoiceWindows() {

        mainApp.showChoices();
    }


    @FXML
    private void handleSymtom() {
        mainApp.showChooseSymptomDialog();

    }
}

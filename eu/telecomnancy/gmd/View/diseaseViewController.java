package eu.telecomnancy.gmd.View;


import eu.telecomnancy.gmd.Data_Searchers.ParserANDOR;
import eu.telecomnancy.gmd.Data_Searchers.Request_Results;
import eu.telecomnancy.gmd.GUIApp;
import eu.telecomnancy.gmd.objects.AdverseEffectDrug;
import eu.telecomnancy.gmd.objects.Symptom;
import eu.telecomnancy.gmd.objects.TreatDrug;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;

public class diseaseViewController {

    @FXML
    private Label diseaseLabel;


    @FXML
    private  ListView listOMIM_ORPHA;
    @FXML
    private  ListView listOMIM;
    @FXML
    private  ListView listORPHA;

    @FXML
    private  ListView listTreatDrugDrugBank_Sider2;
    @FXML
    private  ListView listTreatDrugDrugBank;
    @FXML
    private  ListView listTreatDrugSider2;

    @FXML
    private  ListView listAdverseEffectDrugDrugBank_Sider2;
    @FXML
    private  ListView listAdverseEffectDrugDrugBank;
    @FXML
    private  ListView listAdverseEffectDrugSider2;

    @FXML
    private  TitledPane paneOMIM_ORPHA;
    @FXML
    private  TitledPane paneOMIM;
    @FXML
    private  TitledPane paneORPHA;

    @FXML
    private  TitledPane paneTreatDrugDrugBank_Sider2;
    @FXML
    private  TitledPane paneTreatDrugDrugBank;
    @FXML
    private  TitledPane paneTreatDrugSider2;

    @FXML
    private  TitledPane paneAdverseEffectDrugDrugBank_Sider2;
    @FXML
    private  TitledPane paneAdverseEffectDrugDrugBank;
    @FXML
    private  TitledPane paneAdverseEffectDrugSider2;




    // Reference to the main application.
    private GUIApp mainApp;

    private String disease;
    private ParserANDOR parserANDOR=new ParserANDOR();
    private Request_Results request_results;
    private ObservableList<String> symptomListOMIM_ORPHA=FXCollections.observableArrayList ();
    private ObservableList<String> symptomListOMIM=FXCollections.observableArrayList ();
    private ObservableList<String> symptomListORPHA=FXCollections.observableArrayList ();

    private ObservableList<String> treatDrugListDrugBank_Sider2=FXCollections.observableArrayList ();
    private ObservableList<String> treatDrugListDrugBank=FXCollections.observableArrayList ();
    private ObservableList<String> treatDrugListSider2=FXCollections.observableArrayList ();

    private ObservableList<String> adverseEffectDrugListDrugBank_Sider2=FXCollections.observableArrayList ();
    private ObservableList<String> adverseEffectDrugListDrugBank=FXCollections.observableArrayList ();
    private ObservableList<String> adverseEffectDrugListSider2=FXCollections.observableArrayList ();


    /*
         * The constructor.
         * The constructor is called before the initialize() method.
         */
    public diseaseViewController() {

    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {


    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(GUIApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table

    }
    public void setDisease(String disease){
        this.disease=disease;
        diseaseLabel.setText(this.disease);

    }

    public void getData() throws Exception {
        request_results=parserANDOR.requestORParser(this.disease);

        for (Symptom symptom:request_results.getSymptoms()){
            if(symptom.getSource().size()==2){
                symptomListOMIM_ORPHA.add(symptom.getName()+"  "+symptom.getDisease().toString());
            }else if(symptom.getSource().get(0).equals("orpha")){
                symptomListORPHA.add(symptom.getName()+"  "+symptom.getDisease().toString());
            }else if(symptom.getSource().get(0).equals("OMIM")){
                symptomListOMIM.add(symptom.getName()+"  "+symptom.getDisease().toString());
            }

        }
        for (TreatDrug drug:request_results.getTreats()){
            if(drug.getSource().size()==2){
                treatDrugListDrugBank_Sider2.add(drug.getName()+"  "+drug.getDisease().toString());
            }else if(drug.getSource().get(0).equals("Drugbank")){
                treatDrugListDrugBank.add(drug.getName()+"     "+drug.getDisease().toString());
            }else if(drug.getSource().get(0).equals("sider2")){
                treatDrugListSider2.add(drug.getName()+"     "+drug.getDisease().toString());
            }
        }

        for (AdverseEffectDrug drug:request_results.getAdverseEffects()){
            if(drug.getSource().size()==2){
                adverseEffectDrugListDrugBank_Sider2.add(drug.getName()+"     "+drug.getDisease().toString());
            }else if(drug.getSource().get(0).equals("Drugbank")){
                adverseEffectDrugListDrugBank.add(drug.getName()+"     "+drug.getDisease().toString());
            }else if(drug.getSource().get(0).equals("sider2")){
                adverseEffectDrugListSider2.add(drug.getName()+"     "+drug.getDisease().toString());
            }
        }



        listOMIM_ORPHA.setItems(symptomListOMIM_ORPHA);
        paneOMIM_ORPHA.setText("OMIM & Orpha ("+symptomListOMIM_ORPHA.size()+")");
        listORPHA.setItems(symptomListORPHA);
        paneORPHA.setText("Orpha ("+symptomListORPHA.size()+")");
        listOMIM.setItems(symptomListOMIM);
        paneOMIM.setText("OMIM ("+symptomListOMIM.size()+")");

        listTreatDrugDrugBank_Sider2.setItems(treatDrugListDrugBank_Sider2);
        paneTreatDrugDrugBank_Sider2.setText("DrugBank & Sider2 ("+treatDrugListDrugBank_Sider2.size()+")");
        listTreatDrugDrugBank.setItems(treatDrugListDrugBank);
        paneTreatDrugDrugBank.setText("DrugBank ("+treatDrugListDrugBank.size()+")");
        listTreatDrugSider2.setItems(treatDrugListSider2);
        paneTreatDrugSider2.setText("Sider2 ("+treatDrugListSider2.size()+")");

        listAdverseEffectDrugDrugBank_Sider2.setItems(adverseEffectDrugListDrugBank_Sider2);
        paneAdverseEffectDrugDrugBank_Sider2.setText("DrugBank & Sider2 ("+adverseEffectDrugListDrugBank_Sider2.size()+")");
        listAdverseEffectDrugDrugBank.setItems(adverseEffectDrugListDrugBank);
        paneAdverseEffectDrugDrugBank.setText("DrugBank ("+adverseEffectDrugListDrugBank.size()+")");
        listAdverseEffectDrugSider2.setItems(adverseEffectDrugListSider2);
        paneAdverseEffectDrugSider2.setText("Sider2 ("+adverseEffectDrugListSider2.size()+")");


    }

    @FXML
    private void handleChoiceWindows() {

        mainApp.showChoices();
    }


    @FXML
    private void handleDisease() {
        mainApp.showChooseDiseaseDialog();

    }




}



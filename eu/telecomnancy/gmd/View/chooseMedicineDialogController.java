package eu.telecomnancy.gmd.View;


import eu.telecomnancy.gmd.GUIApp;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class chooseMedicineDialogController {

    @FXML
    private TextField medicineNameField;



    private Stage dialogStage;
    private String medicineName;
    private GUIApp mainApp;


    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;

    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(GUIApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
    }






    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleSearch() {
        this.medicineName=medicineNameField.getText();
        mainApp.showMedicineView(this.medicineName);
        dialogStage.close();
    }


    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }



}

package eu.telecomnancy.gmd;

import java.io.IOException;

import eu.telecomnancy.gmd.View.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class GUIApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

    /**
     * Constructor
     */
    public GUIApp() {
        // Add some sample data

    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("MedicalSearcher App");

        initRootLayout();
        showMainView();


    }


    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIApp.class.getResource("View/rootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            rootLayoutController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */


    public void showChoices() {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIApp.class.getResource("View/choiceWindow.fxml"));
            AnchorPane choices = (AnchorPane) loader.load();


            // Set person overview into the center of root layout.
            rootLayout.setCenter(choices);

            // Give the controller access to the eu.telecomnancy.gmd.main app.
            choiceWindowController controller = loader.getController();
            controller.setMainApp(this);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showMainView(){
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIApp.class.getResource("View/mainView.fxml"));
            AnchorPane mainView = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(mainView);

            // Give the controller access to the eu.telecomnancy.gmd.main app.
            mainViewController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showDiseaseView(String disease){
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIApp.class.getResource("View/diseaseView.fxml"));
            AnchorPane diseaseView = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(diseaseView);

            // Give the controller access to the eu.telecomnancy.gmd.main app.
            diseaseViewController controller = loader.getController();
            controller.setDisease(disease);
            controller.setMainApp(this);
            controller.getData();


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void showSymptomView(String symptom){
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIApp.class.getResource("View/symptomView.fxml"));
            AnchorPane symptomView = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(symptomView);

            // Give the controller access to the eu.telecomnancy.gmd.main app.
            symptomViewController controller = loader.getController();
            controller.setSymptom(symptom);
            controller.setMainApp(this);
            controller.getData();


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showMedicineView(String medicine){
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIApp.class.getResource("View/medicineView.fxml"));
            AnchorPane medicineView = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(medicineView);

            // Give the controller access to the eu.telecomnancy.gmd.main app.
            medicineViewController controller = loader.getController();
            controller.setMedicine(medicine);
            controller.setMainApp(this);
            controller.getData();


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showChooseDiseaseDialog() {
        try {
            // Load the fxml file and create a new stage for the popup.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIApp.class.getResource("View/chooseDiseaseDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Disease");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            chooseDiseaseDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);


            dialogStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showChooseSymptomDialog(){
        try {
            // Load the fxml file and create a new stage for the popup.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIApp.class.getResource("View/chooseSymptomDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Symptoms");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            chooseSymptomDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);


            dialogStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showChooseMedicineDialog(){
        try {
            // Load the fxml file and create a new stage for the popup.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(GUIApp.class.getResource("View/chooseMedicineDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Medicine");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            chooseMedicineDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);


            dialogStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }

}